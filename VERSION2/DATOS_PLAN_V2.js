{
  "application_type": "sql",
  "application_params": {
    "source_params": {
      "adapter": "postgresql",
      "host": "152.139.147.122",
      "port": 5432,
      "database": "eazybi_qa",
      "username": "postgres",
      "password": null,
      "incremental": null,
      "sql": "SELECT\r\n\t\tdwh_22.d_incidente_padre.incidente_padre\r\n\t, \tdwh_22.d_descripcion_origen.descripcion_origen\r\n\t, \tdwh_22.d_estado_incidente_padre.estado_incidente_padre\t\r\n\t, \tdwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica\r\n\t,\tplanificacion.summary\t\r\n\t, \tdwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica\r\n\t,\tplanificacion.\"lead\"\r\n\t, \tdwh_22.d_usuario_lider_proyecto.usuario_lider_proyecto\r\n\t, \tdwh_22.d_incidente_hijo.incidente_hijo\r\n\t,\tplanificacion.codigo\r\n\t, \tdwh_22.d_descripcion_destino.descripcion_destino\r\n\t, \tdwh_22.d_comite_estimado.comite_estimado\r\n\t, \tdwh_22.d_funnel.funnel\r\n\t, \tdwh_22.d_status_avance_color.status_avance_color\r\n\t,\tplanificacion.membername\r\n\t,\tplanificacion.membershiprolename\r\n\t,\tplanificacion.name\r\n\t,\tplanificacion.usuarioasignado\r\n\t,\tplanificacion.usuarioasignadosistema\r\n\t,\tplanificacion.estadoissue\r\n\t,\tplanificacion.diaPlanificado\r\n\t,\tplanificacion.fechainicioplan\r\n\t,\tplanificacion.fechaterminoplan\r\n\t,\tplanificacion.horasplanificadospordia\r\n\t,\tplanificacion.planificacion_original\r\n\r\nFROM\r\n\tdwh_22.c_data_jira\r\n\tLEFT JOIN dwh_22.d_descripcion_origen \t\t\tON dwh_22.c_data_jira.descripcion_origen_id \t\t\t\t= dwh_22.d_descripcion_origen.\"id\"\r\n\tLEFT JOIN dwh_22.d_incidente_hijo \t\t\t\tON dwh_22.c_data_jira.incidente_hijo_id \t\t\t\t\t\t= dwh_22.d_incidente_hijo.\"id\"\r\n\tLEFT JOIN dwh_22.d_descripcion_destino \t\t\tON dwh_22.c_data_jira.descripcion_destino_id \t\t\t\t= dwh_22.d_descripcion_destino.\"id\"\r\n\tLEFT JOIN dwh_22.d_incidente_padre \t\t\t\tON dwh_22.c_data_jira.incidente_padre_id \t\t\t\t\t\t= dwh_22.d_incidente_padre.\"id\"\r\n\tLEFT JOIN dwh_22.d_estado_incidente_padre \t\tON dwh_22.c_data_jira.estado_incidente_padre_id \t\t= dwh_22.d_estado_incidente_padre.\"id\"\r\n\tLEFT JOIN dwh_22.d_estado_incidente_hijo \t\tON dwh_22.c_data_jira.estado_incidente_hijo_id \t\t\t= dwh_22.d_estado_incidente_hijo.\"id\"\r\n\tLEFT JOIN dwh_22.d_usuario_jefe_de_fabrica \t\tON dwh_22.c_data_jira.usuario_jefe_de_fabrica_id \t\t= dwh_22.d_usuario_jefe_de_fabrica.\"id\"\r\n\tLEFT JOIN dwh_22.d_usuario_lider_de_fabrica \tON dwh_22.c_data_jira.usuario_lider_de_fabrica_id \t= dwh_22.d_usuario_lider_de_fabrica.\"id\"\r\n\tLEFT JOIN dwh_22.d_usuario_lider_proyecto \t\tON dwh_22.c_data_jira.usuario_lider_proyecto_id \t\t= dwh_22.d_usuario_lider_proyecto.\"id\"\r\n\tLEFT JOIN dwh_22.d_funnel \t\t\t\t\t\tON dwh_22.c_data_jira.funnel_id \t\t\t\t\t\t\t\t\t\t= dwh_22.d_funnel.\"id\"\r\n\tLEFT JOIN dwh_22.d_comite_estimado \t\t\t\tON dwh_22.c_data_jira.comite_estimado_id \t\t\t\t\t\t= dwh_22.d_comite_estimado.\"id\"\r\n\tLEFT JOIN dwh_22.d_status_avance_color \t\t\tON dwh_22.c_data_jira.status_avance_color_id \t\t\t\t= dwh_22.d_status_avance_color.\"id\"\r\n\tLEFT JOIN \r\n\t(\r\n\t\tSELECT\r\n\t\tdwh_22.d_membername.membername\r\n\t,\tdwh_22.d_membershiprolename.membershiprolename\r\n\t,\tdwh_22.d_name.\"name\"\r\n\t,\tdwh_22.d_summary.summary\r\n\t,\tdwh_22.d_lead.\"lead\"\r\n\t,\tplanificacion.usuarioasignado\r\n\t,\tplanificacion.usuarioasignadosistema\r\n\t,\tplanificacion.estadoissue\r\n\t,\tplanificacion.diaPlanificado\r\n\t,\tplanificacion.fechainicioplan\r\n\t,\tplanificacion.fechaterminoplan\r\n\t,\tplanificacion.horasplanificadospordia\r\n\t,\tplanificacion.planificacion_original\r\n\t,\tplanificacion.codigo\r\n\t\r\n\t\t\tFROM\r\n\t\t\t\tdwh_22.c_miembros\r\n\t\t\t\tINNER JOIN dwh_22.d_membername \t\t\tON dwh_22.c_miembros.membername_id \t\t\t= dwh_22.d_membername.\"id\"\r\n\t\t\t\tINNER JOIN dwh_22.d_membershiprolename \tON dwh_22.c_miembros.membershiprolename_id \t= dwh_22.d_membershiprolename.\"id\"\r\n\t\t\t\tINNER JOIN dwh_22.c_equipos \t\t\tON dwh_22.c_miembros.membershipteam \t\t= dwh_22.c_equipos.\"id\"\r\n\t\t\t\tINNER JOIN dwh_22.d_name \t\t\t\tON dwh_22.c_equipos.name_id \t\t\t\t= dwh_22.d_name.\"id\"\r\n\t\t\t\tINNER JOIN dwh_22.d_summary \t\t\tON dwh_22.c_equipos.summary_id \t\t\t\t= dwh_22.d_summary.\"id\"\r\n\t\t\t\tINNER JOIN dwh_22.d_lead \t\t\t\tON dwh_22.c_equipos.lead_id \t\t\t\t= dwh_22.d_lead.\"id\" \r\n\t\t\t\tLEFT JOIN \r\n\t\t\t\t(\r\n\t\t\t\t\tSELECT\r\n\t\t\t\t\t\tdwh_22.c_planificacion.ID,\r\n\t\t\t\t\t\tdwh_22.d_plan_itemkey.plan_itemkey AS codigo,\r\n\t\t\t\t\t\tdwh_22.d_plan_itemsummary.plan_itemsummary AS Nombre,\r\n\t\t\t\t\t\tdwh_22.d_assigneekey.assigneekey AS UsuarioAsignado,\r\n\t\t\t\t\t\tdwh_22.d_assigneeuser_key.assigneeuser_key AS UsuarioAsignadoSistema,\r\n\t\t\t\t\t\tdwh_22.d_plan_itemissue_statusname.plan_itemissue_statusname AS EstadoIssue,\r\n\t\t\t\t\t\tgenerate_series ( MIN ( dwh_22.d_start.time_value ), MAX ( dwh_22.d_end.time_value ), INTERVAL '24 hour' ) AS diaPlanificado,\r\n\t\t\t\t\t\tdwh_22.d_start.time_value AS FechaInicioPlan,\r\n\t\t\t\t\t\tdwh_22.d_end.time_value AS FechaTerminoPlan,\r\n\t\t\t\t\t\tEXTRACT ( DOW FROM generate_series ( MIN ( dwh_22.d_start.time_value ), MAX ( dwh_22.d_end.time_value ), INTERVAL '24 hour' ) ) AS dw,\r\n\t\t\t\t\t\t((dwh_22.c_planificacion.seconds_per_day)/3600::DECIMAL) AS HorasPlanificadosPorDia,\r\n\t\t\t\t\t\t((dwh_22.c_planificacion.seconds_per_day)/3600::DECIMAL) AS planificacion_original,\r\n\t\t\t\t\t\t(dwh_22.c_planificacion.seconds) AS HorasTotalesPlanificados,\r\n\t\t\t\t\t\t(dwh_22.c_planificacion.plan_itemestimated_remaining_seconds) AS HorasDisponiblesParaEstimacion,\r\n\t\t\t\t\t\tdwh_22.d_created_by.created_by AS PlanificacionCreadaPor_USER,\r\n\t\t\t\t\t\tdwh_22.d_created_by_key.created_by_key AS PlanificacionCreadaPor_KEY \r\n\t\t\t\t\tFROM\r\n\t\t\t\t\t\tdwh_22.c_planificacion\r\n\t\t\t\t\t\tINNER JOIN dwh_22.d_assigneekey \t\t\t\t\t\tON dwh_22.c_planificacion.assigneekey_id \t\t\t\t\t= dwh_22.d_assigneekey.ID \r\n\t\t\t\t\t\tINNER JOIN dwh_22.d_assigneeuser_key \t\t\t\t\tON dwh_22.c_planificacion.assigneeuser_key_id \t\t\t\t= dwh_22.d_assigneeuser_key.ID \r\n\t\t\t\t\t\tINNER JOIN dwh_22.d_plan_itemkey \t\t\t\t\t\tON dwh_22.c_planificacion.plan_itemkey_id \t\t\t\t\t= dwh_22.d_plan_itemkey.ID \r\n\t\t\t\t\t\tINNER JOIN dwh_22.d_plan_itemsummary \t\t\t\t\tON dwh_22.c_planificacion.plan_itemsummary_id \t\t\t\t= dwh_22.d_plan_itemsummary.ID \r\n\t\t\t\t\t\tINNER JOIN dwh_22.d_start_time \t\t\t\t\t\t\tON dwh_22.c_planificacion.start_time_id \t\t\t\t\t= dwh_22.d_start_time.ID \r\n\t\t\t\t\t\tINNER JOIN dwh_22.d_start \t\t\t\t\t\t\t\tON dwh_22.c_planificacion.start_id \t\t\t\t\t\t\t= dwh_22.d_start.ID \r\n\t\t\t\t\t\tINNER JOIN dwh_22.d_end \t\t\t\t\t\t\t\tON dwh_22.c_planificacion.end_id \t\t\t\t\t\t\t= dwh_22.d_end.ID \r\n\t\t\t\t\t\tINNER JOIN dwh_22.d_plan_itemissue_statusname \t\t\tON dwh_22.c_planificacion.plan_itemissue_statusname_id \t\t= d_plan_itemissue_statusname.ID \r\n\t\t\t\t\t\tINNER JOIN dwh_22.d_created_by \t\t\t\t\t\t\tON dwh_22.c_planificacion.created_by_id \t\t\t\t\t= dwh_22.d_created_by.ID \r\n\t\t\t\t\t\tINNER JOIN dwh_22.d_created_by_key \t\t\t\t\t\tON dwh_22.c_planificacion.created_by_key_id \t\t\t\t= dwh_22.d_created_by_key.ID\r\n\t\t\t\t\tWHERE\r\n\t\t\t\t\t\td_plan_itemkey.plan_itemkey LIKE'PMO-%' \r\n\t\t\t\t\tGROUP BY\r\n\t\t\t\t\t\tdwh_22.c_planificacion.ID,\r\n\t\t\t\t\t\tdwh_22.d_plan_itemkey.plan_itemkey,\r\n\t\t\t\t\t\tdwh_22.d_plan_itemsummary.plan_itemsummary,\r\n\t\t\t\t\t\tdwh_22.d_assigneekey.assigneekey,\r\n\t\t\t\t\t\tdwh_22.d_assigneeuser_key.assigneeuser_key,\r\n\t\t\t\t\t\tdwh_22.d_plan_itemissue_statusname.plan_itemissue_statusname,\r\n\t\t\t\t\t\tdwh_22.d_start.time_value,\r\n\t\t\t\t\t\tdwh_22.d_end.time_value,\r\n\t\t\t\t\t\t(dwh_22.c_planificacion.seconds_per_day),\r\n\t\t\t\t\t\t(dwh_22.c_planificacion.seconds),\r\n\t\t\t\t\t\t(dwh_22.c_planificacion.plan_itemestimated_remaining_seconds),\r\n\t\t\t\t\t\tdwh_22.d_created_by.created_by,\r\n\t\t\t\t\t\tdwh_22.d_created_by_key.created_by_key\r\n\t\t\t\t) \r\n\t\t\t\tplanificacion on d_membername.membername = planificacion.UsuarioAsignado\t\t\t\t\t\r\n\t\t\t\twhere planificacion.dw NOT IN ( 6, 0 ) and planificacion.diaPlanificado NOT IN (select feriado from dwh_24.d_feriados)\r\n\r\n\t\t\t\t\r\n\t\t\tgroup by \r\n\t\t\t\tdwh_22.d_membername.membername\r\n\t\t\t\t,\tdwh_22.d_membershiprolename.membershiprolename\r\n\t\t\t\t,\tdwh_22.d_name.\"name\"\r\n\t\t\t\t,\tdwh_22.d_summary.summary\r\n\t\t\t\t,\tdwh_22.d_lead.\"lead\"\r\n\t\t\t\t,\tplanificacion.usuarioasignado\r\n\t\t\t\t,\tplanificacion.usuarioasignadosistema\r\n\t\t\t\t,\tplanificacion.estadoissue\r\n\t\t\t\t,\tplanificacion.diaPlanificado\r\n\t\t\t\t,\tplanificacion.fechainicioplan\r\n\t\t\t\t,\tplanificacion.fechaterminoplan\r\n\t\t\t\t,\tplanificacion.horasplanificadospordia\r\n\t\t\t\t,\tplanificacion.planificacion_original\r\n\t\t\t\t,\tplanificacion.codigo\r\n\t\t\t) as \r\n\t\t\tplanificacion \t\ton \t planificacion.codigo \t\t= dwh_22.d_incidente_hijo.incidente_hijo\r\n\t\t\t\t\t\t\t\t\t\t\t\tand\t planificacion.summary \t= dwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica\r\n\t\t\t\t\t\t\t\t\t\t\t\tand  planificacion.\"lead\" = dwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica\r\n"
    },
    "columns_options": {
      "default_names": true
    },
    "extra_options": {
      "regular_import_frequency": null,
      "regular_import_at": "",
      "time_zone": "Santiago"
    }
  },
  "source_cube_name": "PLAN",
  "columns": [
    {
      "name": "incidente_padre",
      "data_type": "string",
      "dimension": "Incidente Padre"
    },
    {
      "name": "descripcion_origen",
      "data_type": "string",
      "dimension": "Descripcion Origen"
    },
    {
      "name": "estado_incidente_padre",
      "data_type": "string",
      "dimension": "Estado Incidente Padre"
    },
    {
      "name": "usuario_lider_de_fabrica",
      "data_type": "string",
      "dimension": "Usuario Lider De Fabrica"
    },
    {
      "name": "summary",
      "data_type": "string",
      "dimension": "Tempo Jefe Lider Fabrica"
    },
    {
      "name": "usuario_jefe_de_fabrica",
      "data_type": "string",
      "dimension": "Usuario Jefe De Fabrica"
    },
    {
      "name": "lead",
      "data_type": "string",
      "dimension": "Tempo Lider Fabrica"
    },
    {
      "name": "usuario_lider_proyecto",
      "data_type": "string",
      "dimension": "Usuario Lider Proyecto"
    },
    {
      "name": "incidente_hijo",
      "data_type": "string",
      "dimension": "Incidente Hijo"
    },
    {
      "name": "codigo",
      "data_type": "string",
      "dimension": "Tempo IssueKey"
    },
    {
      "name": "descripcion_destino",
      "data_type": "string",
      "dimension": "Descripcion Destino"
    },
    {
      "name": "comite_estimado",
      "data_type": "string",
      "dimension": "Comite Estimado"
    },
    {
      "name": "funnel",
      "data_type": "string",
      "dimension": "Funnel"
    },
    {
      "name": "status_avance_color",
      "data_type": "string",
      "dimension": "Status Avance Color"
    },
    {
      "name": "membername",
      "data_type": "string",
      "dimension": "Usuario Planificado"
    },
    {
      "name": "membershiprolename",
      "data_type": "string",
      "dimension": "Cargo Usuario Planificado"
    },
    {
      "name": "name",
      "data_type": "string",
      "dimension": "Nombre Departamento"
    },
    {
      "name": "usuarioasignado",
      "data_type": "string",
      "dimension": "Usuario Jira"
    },
    {
      "name": "usuarioasignadosistema",
      "data_type": "string",
      "dimension": "Usuario Aplicacion"
    },
    {
      "name": "estadoissue",
      "data_type": "string",
      "dimension": "Estadoissue"
    },
    {
      "name": "diaplanificado",
      "data_type": "datetime",
      "dimension": "Dia Planificado"
    },
    {
      "name": "fechainicioplan",
      "data_type": "datetime",
      "dimension": "Dia Inicio Plan"
    },
    {
      "name": "fechaterminoplan",
      "data_type": "datetime",
      "dimension": "Dia Termino Plan"
    },
    {
      "name": "horasplanificadospordia",
      "data_type": "decimal",
      "scale": 1,
      "dimension": "Measures",
      "dimension_member": "Horas planificadas dia"
    },
    {
      "name": "planificacion_original",
      "data_type": "decimal",
      "scale": 1,
      "dimension": "Measures",
      "dimension_member": "Horas Plan dia linea BASE"
    }
  ]
}