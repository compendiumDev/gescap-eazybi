SELECT
		dwh_22.d_incidente_padre.incidente_padre
	, 	dwh_22.d_descripcion_origen.descripcion_origen
	, 	dwh_22.d_estado_incidente_padre.estado_incidente_padre	
	, 	dwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica
	,	planificacion.summary	
	, 	dwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica
	,	planificacion.lead
	, 	dwh_22.d_usuario_lider_proyecto.usuario_lider_proyecto
	, 	dwh_22.d_incidente_hijo.incidente_hijo
	,	planificacion.parent_key codigo
	, 	dwh_22.d_descripcion_destino.descripcion_destino
	, 	dwh_22.d_comite_estimado.comite_estimado
	, 	dwh_22.d_funnel.funnel
	, 	dwh_22.d_status_avance_color.status_avance_color
	,	planificacion.membername
	,	planificacion.membershiprolename
	,	planificacion.name
	,	planificacion.username usuarioasignado
	,	planificacion.staff usuarioasignadosistema
	--,	planificacion.estadoissue
	,	planificacion.time_value diaPlanificado
	--,	planificacion.fechainicioplan
	--,	planificacion.fechaterminoplan
	,	planificacion.hours horasplanificadospordia
	--,	planificacion.planificacion_original

FROM
	dwh_22.c_data_jira
	LEFT JOIN dwh_22.d_descripcion_origen 				ON dwh_22.c_data_jira.descripcion_origen_id 		= dwh_22.d_descripcion_origen."id"
	LEFT JOIN dwh_22.d_incidente_hijo 					ON dwh_22.c_data_jira.incidente_hijo_id 			= dwh_22.d_incidente_hijo."id"
	LEFT JOIN dwh_22.d_descripcion_destino 				ON dwh_22.c_data_jira.descripcion_destino_id 		= dwh_22.d_descripcion_destino."id"
	LEFT JOIN dwh_22.d_incidente_padre 					ON dwh_22.c_data_jira.incidente_padre_id 			= dwh_22.d_incidente_padre."id"
	LEFT JOIN dwh_22.d_estado_incidente_padre 			ON dwh_22.c_data_jira.estado_incidente_padre_id 	= dwh_22.d_estado_incidente_padre."id"
	LEFT JOIN dwh_22.d_estado_incidente_hijo 			ON dwh_22.c_data_jira.estado_incidente_hijo_id 		= dwh_22.d_estado_incidente_hijo."id"
	LEFT JOIN dwh_22.d_usuario_jefe_de_fabrica 			ON dwh_22.c_data_jira.usuario_jefe_de_fabrica_id 	= dwh_22.d_usuario_jefe_de_fabrica."id"
	LEFT JOIN dwh_22.d_usuario_lider_de_fabrica 		ON dwh_22.c_data_jira.usuario_lider_de_fabrica_id 	= dwh_22.d_usuario_lider_de_fabrica."id"
	LEFT JOIN dwh_22.d_usuario_lider_proyecto 			ON dwh_22.c_data_jira.usuario_lider_proyecto_id 	= dwh_22.d_usuario_lider_proyecto."id"
	LEFT JOIN dwh_22.d_funnel 							ON dwh_22.c_data_jira.funnel_id 					= dwh_22.d_funnel."id"
	LEFT JOIN dwh_22.d_comite_estimado 					ON dwh_22.c_data_jira.comite_estimado_id 			= dwh_22.d_comite_estimado."id"
	LEFT JOIN dwh_22.d_status_avance_color 				ON dwh_22.c_data_jira.status_avance_color_id 		= dwh_22.d_status_avance_color."id"
	LEFT JOIN 
	(
		SELECT
		dwh_22.d_membername.membername
	,	dwh_22.d_membershiprolename.membershiprolename
	,	dwh_22.d_name."name"
	,	dwh_22.d_summary.summary
	,	dwh_22.d_lead."lead"
	,	logeohoras.time_value diaPlanificado
	,	logeohoras.parent_key
	,	logeohoras.issue_key
	,	logeohoras.username
	,	logeohoras.staff
	--,	logeohoras.time_value
	,	logeohoras.hours
	
			FROM
				dwh_22.c_miembros
				INNER JOIN dwh_22.d_membername 			ON dwh_22.c_miembros.membername_id 			= dwh_22.d_membername."id"
				INNER JOIN dwh_22.d_membershiprolename 	ON dwh_22.c_miembros.membershiprolename_id 	= dwh_22.d_membershiprolename."id"
				INNER JOIN dwh_22.c_equipos 			ON dwh_22.c_miembros.membershipteam 		= dwh_22.c_equipos."id"
				INNER JOIN dwh_22.d_name 				ON dwh_22.c_equipos.name_id 				= dwh_22.d_name."id"
				INNER JOIN dwh_22.d_summary 			ON dwh_22.c_equipos.summary_id 				= dwh_22.d_summary."id"
				INNER JOIN dwh_22.d_lead 				ON dwh_22.c_equipos.lead_id 				= dwh_22.d_lead."id" 
				LEFT JOIN 
			 	(
			 			SELECT
			 					dwh_22.d_parent_key.parent_key,
			 					dwh_22.d_issue_key.issue_key,
			 					dwh_22.d_time.time_value,
			 					dwh_22.d_username.username,
			 					dwh_22.d_staff.staff,
			 					dwh_22.c_horasregistradas.hours
			 			FROM
			 				dwh_22.c_horasregistradas
			 				INNER JOIN dwh_22.d_staff 		ON dwh_22.c_horasregistradas.staff_id 		= dwh_22.d_staff."id"
			 				INNER JOIN dwh_22.d_username 	ON dwh_22.c_horasregistradas.username_id 	= dwh_22.d_username."id"
			 				INNER JOIN dwh_22.d_time 		ON dwh_22.c_horasregistradas.time_id 		= dwh_22.d_time."id"
			 				INNER JOIN dwh_22.d_issue_key 	ON dwh_22.c_horasregistradas.issue_key_id 	= dwh_22.d_issue_key."id"
			 				INNER JOIN dwh_22.d_parent_key 	ON dwh_22.c_horasregistradas.parent_key_id 	= dwh_22.d_parent_key."id" 
			 			WHERE
			 				dwh_22.d_issue_key.issue_key LIKE'PMO-%' --and dwh_22.d_parent_key.parent_key = 'PMO-9900'
			 	) 
			 	logeohoras 	--on 	planificacion.codigo 			= logeohoras.parent_key 
			 				on 	dwh_22.d_membername.membername 	= logeohoras.username
			 				--and 	planificacion.diaPlanificado 	= logeohoras.time_value
			 							
				--where planificacion.dw NOT IN ( 6, 0 ) 

				
			group by 
				dwh_22.d_membername.membername
				,	dwh_22.d_membershiprolename.membershiprolename
				,	dwh_22.d_name."name"
				,	dwh_22.d_summary.summary
				,	dwh_22.d_lead."lead"
				,	logeohoras.time_value
				,	logeohoras.parent_key
				,	logeohoras.issue_key
				,	logeohoras.username
				,	logeohoras.staff
				,	logeohoras.time_value
				,	logeohoras.hours
			--ORDER BY
			--	1 ASC
			) as 
			planificacion 		on 	 planificacion.parent_key 		= dwh_22.d_incidente_hijo.incidente_hijo
								and	 planificacion.summary 			= dwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica
								and  planificacion.lead 			= dwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica
