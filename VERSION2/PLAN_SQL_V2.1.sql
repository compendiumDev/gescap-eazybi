SELECT
		dwh_22.d_incidente_padre.incidente_padre
	, 	dwh_22.d_descripcion_origen.descripcion_origen
	, 	dwh_22.d_estado_incidente_padre.estado_incidente_padre	
	, 	dwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica
	,	planificacion.summary	
	, 	dwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica
	,	planificacion."lead"
	, 	dwh_22.d_usuario_lider_proyecto.usuario_lider_proyecto
	, 	dwh_22.d_incidente_hijo.incidente_hijo
	,	planificacion.codigo
	, 	dwh_22.d_descripcion_destino.descripcion_destino
	, 	dwh_22.d_comite_estimado.comite_estimado
	, 	dwh_22.d_funnel.funnel
	, 	dwh_22.d_status_avance_color.status_avance_color
	,	planificacion.membername
	,	planificacion.membershiprolename
	,	planificacion.name
	,	planificacion.usuarioasignado
	,	planificacion.usuarioasignadosistema
	,	planificacion.estadoissue
	,	planificacion.diaPlanificado
	,	planificacion.fechainicioplan
	,	planificacion.fechaterminoplan
	,	planificacion.horasplanificadospordia
	,	planificacion.planificacion_original

FROM
	dwh_22.c_data_jira
	LEFT JOIN dwh_22.d_descripcion_origen 			ON dwh_22.c_data_jira.descripcion_origen_id 				= dwh_22.d_descripcion_origen."id"
	LEFT JOIN dwh_22.d_incidente_hijo 				ON dwh_22.c_data_jira.incidente_hijo_id 						= dwh_22.d_incidente_hijo."id"
	LEFT JOIN dwh_22.d_descripcion_destino 			ON dwh_22.c_data_jira.descripcion_destino_id 				= dwh_22.d_descripcion_destino."id"
	LEFT JOIN dwh_22.d_incidente_padre 				ON dwh_22.c_data_jira.incidente_padre_id 						= dwh_22.d_incidente_padre."id"
	LEFT JOIN dwh_22.d_estado_incidente_padre 		ON dwh_22.c_data_jira.estado_incidente_padre_id 		= dwh_22.d_estado_incidente_padre."id"
	LEFT JOIN dwh_22.d_estado_incidente_hijo 		ON dwh_22.c_data_jira.estado_incidente_hijo_id 			= dwh_22.d_estado_incidente_hijo."id"
	LEFT JOIN dwh_22.d_usuario_jefe_de_fabrica 		ON dwh_22.c_data_jira.usuario_jefe_de_fabrica_id 		= dwh_22.d_usuario_jefe_de_fabrica."id"
	LEFT JOIN dwh_22.d_usuario_lider_de_fabrica 	ON dwh_22.c_data_jira.usuario_lider_de_fabrica_id 	= dwh_22.d_usuario_lider_de_fabrica."id"
	LEFT JOIN dwh_22.d_usuario_lider_proyecto 		ON dwh_22.c_data_jira.usuario_lider_proyecto_id 		= dwh_22.d_usuario_lider_proyecto."id"
	LEFT JOIN dwh_22.d_funnel 						ON dwh_22.c_data_jira.funnel_id 										= dwh_22.d_funnel."id"
	LEFT JOIN dwh_22.d_comite_estimado 				ON dwh_22.c_data_jira.comite_estimado_id 						= dwh_22.d_comite_estimado."id"
	LEFT JOIN dwh_22.d_status_avance_color 			ON dwh_22.c_data_jira.status_avance_color_id 				= dwh_22.d_status_avance_color."id"
	LEFT JOIN 
	(
		SELECT
		dwh_22.d_membername.membername
	,	dwh_22.d_membershiprolename.membershiprolename
	,	dwh_22.d_name."name"
	,	dwh_22.d_summary.summary
	,	dwh_22.d_lead."lead"
	,	planificacion.usuarioasignado
	,	planificacion.usuarioasignadosistema
	,	planificacion.estadoissue
	,	planificacion.diaPlanificado
	,	planificacion.fechainicioplan
	,	planificacion.fechaterminoplan
	,	planificacion.horasplanificadospordia
	,	planificacion.planificacion_original
	,	planificacion.codigo
	
			FROM
				dwh_22.c_miembros
				INNER JOIN dwh_22.d_membername 			ON dwh_22.c_miembros.membername_id 			= dwh_22.d_membername."id"
				INNER JOIN dwh_22.d_membershiprolename 	ON dwh_22.c_miembros.membershiprolename_id 	= dwh_22.d_membershiprolename."id"
				INNER JOIN dwh_22.c_equipos 			ON dwh_22.c_miembros.membershipteam 		= dwh_22.c_equipos."id"
				INNER JOIN dwh_22.d_name 				ON dwh_22.c_equipos.name_id 				= dwh_22.d_name."id"
				INNER JOIN dwh_22.d_summary 			ON dwh_22.c_equipos.summary_id 				= dwh_22.d_summary."id"
				INNER JOIN dwh_22.d_lead 				ON dwh_22.c_equipos.lead_id 				= dwh_22.d_lead."id" 
				LEFT JOIN 
				(
					SELECT
						dwh_22.c_planificacion.ID,
						dwh_22.d_plan_itemkey.plan_itemkey AS codigo,
						dwh_22.d_plan_itemsummary.plan_itemsummary AS Nombre,
						dwh_22.d_assigneekey.assigneekey AS UsuarioAsignado,
						dwh_22.d_assigneeuser_key.assigneeuser_key AS UsuarioAsignadoSistema,
						dwh_22.d_plan_itemissue_statusname.plan_itemissue_statusname AS EstadoIssue,
						generate_series ( MIN ( dwh_22.d_start.time_value ), MAX ( dwh_22.d_end.time_value ), INTERVAL '24 hour' ) AS diaPlanificado,
						dwh_22.d_start.time_value AS FechaInicioPlan,
						dwh_22.d_end.time_value AS FechaTerminoPlan,
						EXTRACT ( DOW FROM generate_series ( MIN ( dwh_22.d_start.time_value ), MAX ( dwh_22.d_end.time_value ), INTERVAL '24 hour' ) ) AS dw,
						((dwh_22.c_planificacion.seconds_per_day)/3600::DECIMAL) AS HorasPlanificadosPorDia,
						((dwh_22.c_planificacion.seconds_per_day)/3600::DECIMAL) AS planificacion_original,
						(dwh_22.c_planificacion.seconds) AS HorasTotalesPlanificados,
						(dwh_22.c_planificacion.plan_itemestimated_remaining_seconds) AS HorasDisponiblesParaEstimacion,
						dwh_22.d_created_by.created_by AS PlanificacionCreadaPor_USER,
						dwh_22.d_created_by_key.created_by_key AS PlanificacionCreadaPor_KEY 
					FROM
						dwh_22.c_planificacion
						INNER JOIN dwh_22.d_assigneekey 						ON dwh_22.c_planificacion.assigneekey_id 					= dwh_22.d_assigneekey.ID 
						INNER JOIN dwh_22.d_assigneeuser_key 					ON dwh_22.c_planificacion.assigneeuser_key_id 				= dwh_22.d_assigneeuser_key.ID 
						INNER JOIN dwh_22.d_plan_itemkey 						ON dwh_22.c_planificacion.plan_itemkey_id 					= dwh_22.d_plan_itemkey.ID 
						INNER JOIN dwh_22.d_plan_itemsummary 					ON dwh_22.c_planificacion.plan_itemsummary_id 				= dwh_22.d_plan_itemsummary.ID 
						INNER JOIN dwh_22.d_start_time 							ON dwh_22.c_planificacion.start_time_id 					= dwh_22.d_start_time.ID 
						INNER JOIN dwh_22.d_start 								ON dwh_22.c_planificacion.start_id 							= dwh_22.d_start.ID 
						INNER JOIN dwh_22.d_end 								ON dwh_22.c_planificacion.end_id 							= dwh_22.d_end.ID 
						INNER JOIN dwh_22.d_plan_itemissue_statusname 			ON dwh_22.c_planificacion.plan_itemissue_statusname_id 		= d_plan_itemissue_statusname.ID 
						INNER JOIN dwh_22.d_created_by 							ON dwh_22.c_planificacion.created_by_id 					= dwh_22.d_created_by.ID 
						INNER JOIN dwh_22.d_created_by_key 						ON dwh_22.c_planificacion.created_by_key_id 				= dwh_22.d_created_by_key.ID
					WHERE
						d_plan_itemkey.plan_itemkey LIKE'PMO-%' 
					GROUP BY
						dwh_22.c_planificacion.ID,
						dwh_22.d_plan_itemkey.plan_itemkey,
						dwh_22.d_plan_itemsummary.plan_itemsummary,
						dwh_22.d_assigneekey.assigneekey,
						dwh_22.d_assigneeuser_key.assigneeuser_key,
						dwh_22.d_plan_itemissue_statusname.plan_itemissue_statusname,
						dwh_22.d_start.time_value,
						dwh_22.d_end.time_value,
						(dwh_22.c_planificacion.seconds_per_day),
						(dwh_22.c_planificacion.seconds),
						(dwh_22.c_planificacion.plan_itemestimated_remaining_seconds),
						dwh_22.d_created_by.created_by,
						dwh_22.d_created_by_key.created_by_key
				) 
				planificacion on d_membername.membername = planificacion.UsuarioAsignado					
				where planificacion.dw NOT IN ( 6, 0 ) and planificacion.diaPlanificado NOT IN (select feriado from dwh_24.d_feriados)

				
			group by 
				dwh_22.d_membername.membername
				,	dwh_22.d_membershiprolename.membershiprolename
				,	dwh_22.d_name."name"
				,	dwh_22.d_summary.summary
				,	dwh_22.d_lead."lead"
				,	planificacion.usuarioasignado
				,	planificacion.usuarioasignadosistema
				,	planificacion.estadoissue
				,	planificacion.diaPlanificado
				,	planificacion.fechainicioplan
				,	planificacion.fechaterminoplan
				,	planificacion.horasplanificadospordia
				,	planificacion.planificacion_original
				,	planificacion.codigo
			) as 
			planificacion 		on 	 planificacion.codigo 		= dwh_22.d_incidente_hijo.incidente_hijo
												and	 planificacion.summary 	= dwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica
												and  planificacion."lead" = dwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica
