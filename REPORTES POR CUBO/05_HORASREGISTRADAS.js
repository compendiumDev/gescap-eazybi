{
  "cube_name": "HORASREGISTRADAS",
  "cube_reports": [ {
     "name": "Horas logeadas JP Mensual",
     "result_view": "table",
     "definition": {"columns":{"dimensions":[{"name":"Measures","selected_set":["[Measures].[Hours]"],"members":[]}]},"rows":{"dimensions":[{"name":"Time","selected_set":["[Time].[All Times]"],"selected_set_expression":"DescendantsSet({{selected_set}}, [Time].[Month])","members":[],"bookmarked_members":[]},{"name":"Parent Key","selected_set":["[Parent Key].[Parent Key].Members"],"members":[],"bookmarked_members":[]}],"nonempty_crossjoin":true},"pages":{"dimensions":[{"name":"Username","selected_set":["[Username].[afhernandr]"],"members":[{"depth":1,"name":"afhernandr","full_name":"[Username].[afhernandr]"}],"bookmarked_members":[],"current_page_members":["[Username].[afhernandr]"]},{"name":"Time","duplicate":true,"selected_set":["[Time].[All Times]"],"members":[{"depth":0,"name":"All Times","full_name":"[Time].[All Times]","drillable":true,"type":"all"}],"bookmarked_members":[],"current_page_members":["[Time].[All Times]"]}]},"options":{"nonempty":true},"view":{"current":"table","maximized":false,"table":{}}}
  },{
     "name": "TES JP HORAS LOGEADAS",
     "result_view": "table",
     "definition": {"columns":{"dimensions":[{"name":"Measures","selected_set":["[Measures].[Worklog]","[Measures].[Jira Worklog]","[Measures].[Issue]","[Measures].[Hours]","[Measures].[External Hours]"],"members":[]}]},"rows":{"dimensions":[{"name":"Issue Key","selected_set":["[Issue Key].[All Issue Keys]"],"members":[],"bookmarked_members":[]},{"name":"Username","selected_set":["[Username].[All Usernames]"],"members":[],"bookmarked_members":[]}]},"pages":{"dimensions":[]},"options":{},"view":{"current":"table","maximized":false,"table":{}}}
  } ],
  "calculated_members": []
}
