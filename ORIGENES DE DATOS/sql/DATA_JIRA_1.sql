/*
Nombre CUBO : DATA_JIRA_1
Servidor : 152.139.147.122
puerto : 5432
base de datos : jira_qa
usuario : postgres
Cliente : BCH-GESCAP
*/

SELECT
	--"details".fila_unica,
	issuelink."id"																							as "fila_unica",
	issuelinktype.linkname																			as "Link Origen",
  issue_origen."id" 																					as "id origen",
	--issue_origen.project											as "Projecto origen",
	concat('PMO-',issue_origen.issuenum)												as "Incidente Padre",
	issue_destino."id"																					as "id destino",
	issue_origen.summary																				as "Descripcion origen",
	estado_origen.pname																					as "Estado Incidente Padre",
	comite.customvalue																					as "Comite Estimado",
	funnel.customvalue																					as "Funnel",
	"liderproyecto".stringvalue 																as "Usuario Lider Proyecto",
	"jefefabrica".stringvalue 																	as "Usuario Jefe de Fabrica",
	"liderjefefabrica".stringvalue															as "Usuario Lider de Fabrica",
	concat('PMO-',issue_destino.issuenum)												as "Incidente hijo",
	issue_destino.summary																				as "Descripcion destino",
	estado_destino.pname																				as "Estado Incidente hijo",
	"details".linkname,
  "details".taskid,
	--"details".project, 
	concat('PMO-',"details".tarea_origen)												as "Incidente hijoSubIncidente",
	"details".nombreIncidenciaPadre															as "nombre tarea",
	"planoriginal".stringvalue 																	as "planificacion original",
	"details".subtaskid																					as "id tarea",
	--"details".project																						as , 
	concat('PMO-',"details".tarea_destino)											as "id subtarea",
	"details".nombreIncidenciaHijo															as "nombre subtask",
	"details"."estadoSubTask"
FROM issuelink
	INNER JOIN issuelinktype ON issuelink.linktype = issuelinktype."id"
	INNER JOIN jiraissue AS issue_origen ON CAST(coalesce(issuelink."source", '0') AS integer) = issue_origen."id"
	INNER JOIN issuestatus as estado_origen on issue_origen.issuestatus = estado_origen."id"
	INNER JOIN jiraissue AS issue_destino ON CAST(coalesce(issuelink.destination, '0') AS integer) = issue_destino."id"
	INNER JOIN issuestatus as estado_destino on issue_destino.issuestatus = estado_destino."id"
	INNER JOIN (SELECT
								issuelink."id" as fila_unica,
								issuelinktype.linkname,
								issue_origen."id" as "taskid",
								issue_origen.project, 
								issue_origen.issuenum as "tarea_origen",
								issue_origen.summary as nombreIncidenciaPadre,
								issue_destino."id" as "subtaskid",
								issue_destino.project, 
								issue_destino.issuenum as "tarea_destino",
								issue_destino.summary as nombreIncidenciaHijo,
								 estado_destino.pname as "estadoSubTask"
							FROM
								issuelink INNER JOIN issuelinktype ON issuelink.linktype = issuelinktype."id"
													INNER JOIN jiraissue AS issue_origen ON CAST(coalesce(issuelink."source", '0') AS integer) = issue_origen."id"
														INNER JOIN issuestatus as estado_origen on issue_origen.issuestatus = estado_origen."id"

													INNER JOIN jiraissue AS issue_destino ON CAST(coalesce(issuelink.destination, '0') AS integer) = issue_destino."id"
														INNER JOIN issuestatus as estado_destino on issue_destino.issuestatus = estado_destino."id"

							where issue_origen.project = 17400 and issue_destino.project = 17400 and issuelinktype.linkname = 'jira_subtask_link' 
) 
AS "details" ON CAST(coalesce(issuelink."destination", '0') AS integer) = "details".taskid
	LEFT JOIN (
										select
								CFV.stringvalue
								, CF.cfname
								, JI."id" as taskid
								from CustomFieldValue CFV
								inner join CustomField CF on CFV.CustomField = CF.Id
								inner join JiraIssue JI on CFV.Issue = JI.Id
								inner join Project P on JI.Project = P.Id
								where CF.cfname = 'PlanificacionTotal'
	) 
AS planoriginal ON CAST(coalesce(issuelink."destination", '0') AS integer) = "planoriginal".taskid
	LEFT JOIN (
										select
								CFV.stringvalue
								, CF.cfname
								, JI."id" as taskid
								from CustomFieldValue CFV
								inner join CustomField CF on CFV.CustomField = CF.Id
								inner join JiraIssue JI on CFV.Issue = JI.Id
								inner join Project P on JI.Project = P.Id
								where CF.cfname = 'Líder Proyecto'
	) 
AS liderproyecto ON CAST(coalesce(issuelink."destination", '0') AS integer) = "liderproyecto".taskid
	LEFT JOIN (
										select
								CFV.stringvalue
								, CF.cfname
								, JI."id" as taskid
								from CustomFieldValue CFV
								inner join CustomField CF on CFV.CustomField = CF.Id
								inner join JiraIssue JI on CFV.Issue = JI.Id
								inner join Project P on JI.Project = P.Id
								where CF.cfname = 'Líder Jefe Fábrica'
	) 
AS liderjefefabrica ON CAST(coalesce(issuelink."destination", '0') AS integer) = "liderjefefabrica".taskid
	LEFT JOIN (
											select
									CFV.stringvalue
									, CF.cfname
									, JI."id" as taskid
									from CustomFieldValue CFV
									inner join CustomField CF on CFV.CustomField = CF.Id
									inner join JiraIssue JI on CFV.Issue = JI.Id
									inner join Project P on JI.Project = P.Id
									where CF.cfname = 'Jefe Fábrica'
		) 
AS jefefabrica ON CAST(coalesce(issuelink."destination", '0') AS integer) = "jefefabrica".taskid
	JOIN (
		select
					CFV.stringvalue
					, CF.cfname
					, CFO.customvalue
					, P.PKey || '-' || JI.IssueNum as "numeroTicket"
					from CustomFieldValue CFV
					inner join CustomField CF on CFV.CustomField = CF.Id
					inner join JiraIssue JI on CFV.Issue = JI.Id
					inner join Project P on JI.Project = P.Id
					left join customfieldoption CFO on cfo.id::VARCHAR = CFV.stringvalue::VARCHAR
					where CF.cfname = 'Comite Estimado' 
	)
AS comite ON concat('PMO-',issue_origen.issuenum) = comite."numeroTicket"
left JOIN (
		select
					CFV.stringvalue
					, CF.cfname
					, CFO.customvalue
					, P.PKey || '-' || JI.IssueNum as "numeroTicket"
					from CustomFieldValue CFV
					inner join CustomField CF on CFV.CustomField = CF.Id
					inner join JiraIssue JI on CFV.Issue = JI.Id
					inner join Project P on JI.Project = P.Id
					left join customfieldoption CFO on cfo.id::VARCHAR = CFV.stringvalue::VARCHAR
					where CF.cfname = 'Funnel' 
	)
AS funnel ON concat('PMO-',issue_origen.issuenum) = funnel."numeroTicket"

where issue_origen.project = 17400 and issue_destino.project = 17400 and issuelinktype.linkname = 'Epic-Story Link' and issue_destino.summary like '[CAP] %'

order by issuelink."id" asc
;