/*
Nombre CUBO : BASE2
Servidor : 152.139.147.122
puerto : 5432
base de datos : eazybi_qa
usuario : postgres
Cliente : BCH-GESCAP
*/

WITH days AS (
	SELECT
		dwh_21.d_name."name" AS "Departamento"
	,	dwh_21.d_membername.membername AS "UsuarioJira"
	,	dwh_21.d_memberdisplayname.memberdisplayname "Nombre Completo"
	,	dwh_21.c_miembros.membershipavailability "% Disponibilidad diaria"
	, dwh_21.d_membership_beanrolename.membership_beanrolename AS "Rol"
	,	generate_series ( MIN ( "planificacion"."Fecha inicio plan" ), MAX ( "planificacion"."Fecha termino plan" ), INTERVAL '24 hour' ) AS "dia planificado"
	,	EXTRACT ( DOW FROM generate_series ( MIN ( "planificacion"."Fecha inicio plan" ), MAX ( "planificacion"."Fecha termino plan" ), INTERVAL '24 hour' ) ) AS dw
	,	( ( 8 * dwh_21.c_miembros.membershipavailability ) / 100 ) "horas Disponibles dia"
	,	"planificacion"."Fecha inicio plan"
	,	"planificacion"."Fecha termino plan"
	,	"planificacion"."Horas planificadas por dia"
	,	"jira".planificacion_original
	,	"planificacion"."codigo"
	,	"planificacion"."Nombre"
	,	"planificacion"."Usuario Asignado"
	,	"planificacion"."Usuario Asignado Sistema"
	,	"planificacion"."Estado Issue"
	,	"planificacion"."Planificacion creada por (USER)"
	,	"planificacion"."Planificacion creada por (KEY)"
	,	"jira".incidente_padre
	,	"jira".descripcion_origen
	,	"jira".estado_incidente_padre
	,	"jira".usuario_jefe_de_fabrica
	,	"jira".usuario_lider_de_fabrica
	,	"jira".usuario_lider_proyecto
	,	"jira".incidente_hijo
	,	"jira".descripcion_destino
	,	"jira".comite_estimado
	,	"jira".funnel 
	FROM
		dwh_21.c_miembros
		INNER JOIN dwh_21.c_equipos ON dwh_21.c_miembros.membership_beanteam = c_equipos."id"
		INNER JOIN dwh_21.d_name ON dwh_21.c_equipos.name_id = d_name."id"
		INNER JOIN dwh_21.d_membername ON dwh_21.c_miembros.membername_id = d_membername."id"
		INNER JOIN dwh_21.d_memberdisplayname ON dwh_21.c_miembros.memberdisplayname_id = dwh_21.d_memberdisplayname."id"
		INNER JOIN dwh_21.d_membership_beanrolename ON dwh_21.c_miembros.membership_beanrolename_id = dwh_21.d_membership_beanrolename."id"
		INNER JOIN (
		SELECT
			dwh_21.c_planificacion."id",
			dwh_21.d_plan_itemkey.plan_itemkey AS "codigo",
			dwh_21.d_plan_itemsummary.plan_itemsummary AS "Nombre",
			dwh_21.d_assigneekey.assigneekey AS "Usuario Asignado",
			dwh_21.d_assigneeuser_key.assigneeuser_key AS "Usuario Asignado Sistema",
			dwh_21.d_plan_itemissue_statusname.plan_itemissue_statusname AS "Estado Issue",
			dwh_21.d_start.time_value AS "Fecha inicio plan",
			dwh_21.d_end.time_value AS "Fecha termino plan",
			( dwh_21.c_planificacion.seconds_per_day)/3600::DECIMAL AS "Horas planificadas por dia",
			( dwh_21.c_planificacion.seconds / 3600 )::DECIMAL	AS "Horas totales planificados",
			( dwh_21.c_planificacion.plan_itemestimated_remaining_seconds / 3600 )::DECIMAL AS "Horas disponibles para estimacion",
			dwh_21.d_created_by.created_by AS "Planificacion creada por (USER)",
			dwh_21.d_created_by_key.created_by_key AS "Planificacion creada por (KEY)" 
		FROM
			dwh_21.c_planificacion
			INNER JOIN dwh_21.d_assigneekey ON dwh_21.c_planificacion.assigneekey_id = dwh_21.d_assigneekey."id"
			INNER JOIN dwh_21.d_assigneeuser_key ON dwh_21.c_planificacion.assigneeuser_key_id = dwh_21.d_assigneeuser_key."id"
			INNER JOIN dwh_21.d_plan_itemkey ON dwh_21.c_planificacion.plan_itemkey_id = dwh_21.d_plan_itemkey."id"
			INNER JOIN dwh_21.d_plan_itemsummary ON dwh_21.c_planificacion.plan_itemsummary_id = dwh_21.d_plan_itemsummary."id"
			INNER JOIN dwh_21.d_starttime ON dwh_21.c_planificacion.starttime_id = dwh_21.d_starttime."id"
			INNER JOIN dwh_21.d_start ON dwh_21.c_planificacion.start_id = dwh_21.d_start."id"
			INNER JOIN dwh_21.d_end ON dwh_21.c_planificacion.end_id = dwh_21.d_end."id"
			INNER JOIN dwh_21.d_plan_itemissue_statusname ON dwh_21.c_planificacion.plan_itemissue_statusname_id = d_plan_itemissue_statusname."id"
			INNER JOIN dwh_21.d_created_by ON dwh_21.c_planificacion.created_by_id = dwh_21.d_created_by."id"
			INNER JOIN dwh_21.d_created_by_key ON dwh_21.c_planificacion.created_by_key_id = dwh_21.d_created_by_key."id" 
		WHERE
			d_plan_itemkey.plan_itemkey LIKE'PMO-%' --and dwh_21.d_plan_itemkey.plan_itemkey = 'PMO-10332' 
		) AS "planificacion" ON dwh_21.d_membername.membername = "planificacion"."Usuario Asignado"
		LEFT JOIN (
		SELECT
			dwh_21.d_incidente_padre.incidente_padre,
			dwh_21.d_descripcion_origen.descripcion_origen,
			dwh_21.d_estado_incidente_padre.estado_incidente_padre,
			dwh_21.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica,
			dwh_21.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica,
			dwh_21.d_usuario_lider_proyecto.usuario_lider_proyecto,
			dwh_21.d_incidente_hijo.incidente_hijo,
			dwh_21.d_descripcion_destino.descripcion_destino,
			((dwh_21.d_planificacion_original.planificacion_original::DECIMAL/3600)/8) as planificacion_original,
			dwh_21.d_comite_estimado.comite_estimado,
			dwh_21.d_funnel.funnel 
		FROM
			dwh_21.c_data_jira
			LEFT JOIN dwh_21.d_descripcion_origen ON dwh_21.c_data_jira.descripcion_origen_id = dwh_21.d_descripcion_origen."id"
			LEFT JOIN dwh_21.d_incidente_hijo ON dwh_21.c_data_jira.incidente_hijo_id = dwh_21.d_incidente_hijo."id"
			LEFT JOIN dwh_21.d_descripcion_destino ON dwh_21.c_data_jira.descripcion_destino_id = dwh_21.d_descripcion_destino."id"
			LEFT JOIN dwh_21.d_incidente_padre ON dwh_21.c_data_jira.incidente_padre_id = dwh_21.d_incidente_padre."id"
			LEFT JOIN dwh_21.d_estado_incidente_padre ON dwh_21.c_data_jira.estado_incidente_padre_id = dwh_21.d_estado_incidente_padre."id"
			LEFT JOIN dwh_21.d_estado_incidente_hijo ON dwh_21.c_data_jira.estado_incidente_hijo_id = dwh_21.d_estado_incidente_hijo."id"
			LEFT JOIN dwh_21.d_planificacion_original ON dwh_21.c_data_jira.planificacion_original_id = dwh_21.d_planificacion_original."id"
			LEFT JOIN dwh_21.d_usuario_jefe_de_fabrica ON dwh_21.c_data_jira.usuario_jefe_de_fabrica_id = dwh_21.d_usuario_jefe_de_fabrica."id"
			LEFT JOIN dwh_21.d_usuario_lider_de_fabrica ON dwh_21.c_data_jira.usuario_lider_de_fabrica_id = dwh_21.d_usuario_lider_de_fabrica."id"
			LEFT JOIN dwh_21.d_usuario_lider_proyecto ON dwh_21.c_data_jira.usuario_lider_proyecto_id = dwh_21.d_usuario_lider_proyecto."id"
			LEFT JOIN dwh_21.d_funnel ON dwh_21.c_data_jira.funnel_id = dwh_21.d_funnel."id"
			LEFT JOIN dwh_21.d_comite_estimado ON dwh_21.c_data_jira.comite_estimado_id = dwh_21.d_comite_estimado."id" 
		) AS "jira" ON "planificacion".codigo = "jira".incidente_hijo 
	GROUP BY
		dwh_21.c_equipos."id"
		,	dwh_21.d_name."name"
		,	dwh_21.d_membername.membername
		,	dwh_21.d_memberdisplayname.memberdisplayname
		,	dwh_21.c_miembros.membershipavailability
		, dwh_21.d_membership_beanrolename.membership_beanrolename
		,	( ( 8 * dwh_21.c_miembros.membershipavailability ) / 100 )
		,	"planificacion"."id"
		,	"planificacion"."codigo"
		,	"planificacion"."Nombre"
		,	"planificacion"."Usuario Asignado"
		,	"planificacion"."Usuario Asignado Sistema"
		,	"planificacion"."Estado Issue"
		,	"planificacion"."Fecha inicio plan"
		,	"planificacion"."Fecha termino plan"
		,	"planificacion"."Horas planificadas por dia"
		,	"jira".planificacion_original
		,	"planificacion"."Planificacion creada por (USER)"
		,	"planificacion"."Planificacion creada por (KEY)"
		,	"jira".incidente_padre
		,	"jira".descripcion_origen
		,	"jira".estado_incidente_padre
		,	"jira".usuario_jefe_de_fabrica
		,	"jira".usuario_lider_de_fabrica
		,	"jira".usuario_lider_proyecto
		,	"jira".incidente_hijo
		,	"jira".descripcion_destino
		,	"jira".comite_estimado
		,	"jira".funnel 
	) SELECT
	* 
FROM
	days 
WHERE
	dw NOT IN ( 6, 0 )