/*
Nombre CUBO : RECURSOS CAPACIDAD
Servidor : 152.139.147.122
puerto : 5432
base de datos : eazybi_qa
usuario : postgres
Cliente : BCH-GESCAP
*/

select 
	teams."summary" as "Usuario lider de fabrics"
,	teams."name" as "Departamento"
,	teams."lead" as "Usuario Jefe Fabrica"
, teams.membership_beanrolename as "Rol"
, count(teams.membername) as "Cantidad"
	
from 
(
SELECT
	dwh_21.c_equipos."id" AS "idEquipo",
	dwh_21.d_membername."id" AS "idUsuario",
	dwh_21.d_name."name",
	dwh_21.d_membername.membername,
	dwh_21.d_memberdisplayname.memberdisplayname,
	dwh_21.d_lead."lead",
	dwh_21.d_membership_beanrolename.membership_beanrolename,
	dwh_21.d_summary.summary,
	dwh_21.c_miembros.membershipavailability
FROM
	dwh_21.c_miembros
	INNER JOIN dwh_21.c_equipos 									ON dwh_21.c_miembros.membership_beanteam 				= dwh_21.c_equipos."id"
	INNER JOIN dwh_21.d_name 											ON dwh_21.c_equipos.name_id 										= dwh_21.d_name."id"
	INNER JOIN dwh_21.d_membername 								ON dwh_21.c_miembros.membername_id 							= dwh_21.d_membername."id"
	INNER JOIN dwh_21.d_memberdisplayname 				ON dwh_21.c_miembros.memberdisplayname_id 			= dwh_21.d_memberdisplayname."id"
	INNER JOIN dwh_21.d_lead 											ON dwh_21.c_equipos.lead_id 										= dwh_21.d_lead."id"
	INNER JOIN dwh_21.d_membership_beanrolename 	ON dwh_21.c_miembros.membership_beanrolename_id = dwh_21.d_membership_beanrolename."id"
	INNER JOIN dwh_21.d_summary 									ON dwh_21.c_equipos.summary_id 									= dwh_21.d_summary."id" 
) teams

group by 
	teams."name" 
,	teams."lead" 
,	teams."summary"
, teams.membership_beanrolename 