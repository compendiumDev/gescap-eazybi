/*
Nombre CUBO : LOGEO_HORAS
Servidor : 152.139.147.122
puerto : 5432
base de datos : eazybi_qa
usuario : postgres
Cliente : BCH-GESCAP
*/

select "max"(logeo.worklog) id , logeo.username, dataplan.UsuarioAsignadoSistema ,  logeo.fecha, logeo.issue_key, logeo.horas from 

(
SELECT DISTINCT ON (dwh_21.c_horasregistradas.worklog)
	dwh_21.c_horasregistradas.worklog,
	dwh_21.d_staff.staff,
	dwh_21.d_username.username,
	dwh_21.d_time.time_value as fecha,
	dwh_21.d_issue_key.issue_key,
	SUM ( dwh_21.c_horasregistradas.hours ) AS horas 
FROM
	dwh_21.c_horasregistradas
	INNER JOIN dwh_21.d_staff 			ON dwh_21.c_horasregistradas.staff_id 			= dwh_21.d_staff."id"
	INNER JOIN dwh_21.d_username 		ON dwh_21.c_horasregistradas.username_id 		= dwh_21.d_username."id"
	INNER JOIN dwh_21.d_time 				ON dwh_21.c_horasregistradas.time_id 			= dwh_21.d_time."id"
	INNER JOIN dwh_21.d_issue_key 	ON dwh_21.c_horasregistradas.issue_key_id 		= dwh_21.d_issue_key."id" 
WHERE
	dwh_21.d_issue_key.issue_key LIKE 'PMO-%' --and d_issue_key.issue_key = 'PMO-9417'
	
	group by
	dwh_21.c_horasregistradas.worklog,
	dwh_21.d_staff.staff, 
	dwh_21.d_username.username, 
	dwh_21.d_time.time_value, 
	dwh_21.d_issue_key.issue_key
		
	) logeo 
	LEFT JOIN 
		(
		SELECT
	dwh_21.d_assigneekey.assigneekey 													AS UsuarioAsignado,
	dwh_21.d_assigneeuser_key.assigneeuser_key 								AS UsuarioAsignadoSistema
FROM
	dwh_21.c_planificacion
	INNER JOIN dwh_21.d_assigneekey ON dwh_21.c_planificacion.assigneekey_id = dwh_21.d_assigneekey.id
	INNER JOIN dwh_21.d_assigneeuser_key ON dwh_21.c_planificacion.assigneeuser_key_id = dwh_21.d_assigneeuser_key.id
	INNER JOIN dwh_21.d_plan_itemkey ON dwh_21.c_planificacion.plan_itemkey_id = dwh_21.d_plan_itemkey.id
	INNER JOIN dwh_21.d_plan_itemsummary ON dwh_21.c_planificacion.plan_itemsummary_id = dwh_21.d_plan_itemsummary.id
	INNER JOIN dwh_21.d_starttime ON dwh_21.c_planificacion.starttime_id = dwh_21.d_starttime.id
	INNER JOIN dwh_21.d_start ON dwh_21.c_planificacion.start_id = dwh_21.d_start.id
	INNER JOIN dwh_21.d_end ON dwh_21.c_planificacion.end_id = dwh_21.d_end.id
	INNER JOIN dwh_21.d_plan_itemissue_statusname ON dwh_21.c_planificacion.plan_itemissue_statusname_id = d_plan_itemissue_statusname.id
	INNER JOIN dwh_21.d_created_by ON dwh_21.c_planificacion.created_by_id = dwh_21.d_created_by.id
	INNER JOIN dwh_21.d_created_by_key ON dwh_21.c_planificacion.created_by_key_id = dwh_21.d_created_by_key.id 
WHERE
	d_plan_itemkey.plan_itemkey LIKE 'PMO-%' 
	
group by dwh_21.d_assigneekey.assigneekey,
	dwh_21.d_assigneeuser_key.assigneeuser_key
		) as dataplan on logeo.username = dataplan.UsuarioAsignado

	
	GROUP BY logeo.username,dataplan.UsuarioAsignadoSistema,  logeo.fecha, logeo.issue_key, logeo.horas
	ORDER BY logeo.fecha