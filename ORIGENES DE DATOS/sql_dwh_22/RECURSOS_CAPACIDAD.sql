/*
Nombre CUBO : RECURSOS CAPACIDAD
Servidor : 152.139.147.122
puerto : 5432
base de datos : eazybi_qa
usuario : postgres
Cliente : BCH-GESCAP
*/

select 
	teams."summary" as "Usuario lider de fabric"
,	teams."name" as "Departamento"
,	teams."lead" as "Usuario Jefe de fabrica"
, teams.membership_beanrolename as "Rol"
, count(teams.membername) as "Cantidad"
	
from 
(
SELECT
	dwh_22.c_equipos."id" AS "idEquipo",
	dwh_22.d_membername."id" AS "idUsuario",
	dwh_22.d_name."name",
	dwh_22.d_membername.membername,
	dwh_22.d_memberdisplayname.memberdisplayname,
	dwh_22.d_lead."lead",
	dwh_22.d_membership_beanrolename.membership_beanrolename,
	dwh_22.d_summary.summary,
	dwh_22.c_miembros.membershipavailability
FROM
	dwh_22.c_miembros
	INNER JOIN dwh_22.c_equipos 									ON dwh_22.c_miembros.membership_beanteam 				= dwh_22.c_equipos."id"
	INNER JOIN dwh_22.d_name 											ON dwh_22.c_equipos.name_id 										= dwh_22.d_name."id"
	INNER JOIN dwh_22.d_membername 								ON dwh_22.c_miembros.membername_id 							= dwh_22.d_membername."id"
	INNER JOIN dwh_22.d_memberdisplayname 				ON dwh_22.c_miembros.memberdisplayname_id 			= dwh_22.d_memberdisplayname."id"
	INNER JOIN dwh_22.d_lead 											ON dwh_22.c_equipos.lead_id 										= dwh_22.d_lead."id"
	INNER JOIN dwh_22.d_membership_beanrolename 	ON dwh_22.c_miembros.membership_beanrolename_id = dwh_22.d_membership_beanrolename."id"
	INNER JOIN dwh_22.d_summary 									ON dwh_22.c_equipos.summary_id 									= dwh_22.d_summary."id" 
) teams

group by 
	teams."name" 
,	teams."lead" 
,	teams."summary"
, teams.membership_beanrolename 