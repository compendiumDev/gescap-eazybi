/*
Nombre CUBO : base
Servidor : 152.139.147.122
puerto : 5432
base de datos : eazybi_qa
usuario : postgres
Cliente : BCH-GESCAP
*/
SELECT
  dwh_22.c_planificacion."id",
	dwh_22.d_plan_itemkey.plan_itemkey 															as "codigo", 
	dwh_22.d_plan_itemsummary.plan_itemsummary													as "Nombre",
	dwh_22.d_assigneekey.assigneekey															as "Usuario Asignado", 
	dwh_22.d_assigneeuser_key.assigneeuser_key													as "Usuario Asignado Sistema", 
	dwh_22.d_plan_itemissue_statusname.plan_itemissue_statusname  								as "Estado Issue",
		team.membership_beanrolename															as "Rol",
  	team.membershipavailability																	as "% Asignado",
		team."Nombre Departamento",
	dwh_22.d_start.time_value																	as "Fecha inicio plan", 
	dwh_22.d_end.time_value 																	as "Fecha termino plan", 
	dwh_22.c_planificacion.seconds_per_day														as "Segundos planificados por dia", 
	dwh_22.c_planificacion.seconds																as "Segundos totales planificados", 
	dwh_22.c_planificacion.plan_itemestimated_remaining_seconds									as "Segundos disponibles para estimacion",
		incidentes.planificacion_original														as "Planificacion Base",
		worklog.hours																			as "Horas registradas",
	dwh_22.d_created_by.created_by																as "Planificacion creada por (USER)", 
	dwh_22.d_created_by_key.created_by_key														as "Planificacion creada por (KEY)",
		incidentes.incidente_padre																as "KEY PROJECTO",
		incidentes.descripcion_origen															as "DESC PROJECTO",
		incidentes.estado_incidente_padre														as "ESTADO PROJECTO",
		incidentes.incidente_hijo																as "KEY TAREA",
		incidentes.descripcion_destino															as "DESC TAREA",
		--incidentes.estado_incidente_hijo														as "ESTADO TAREA",
		--incidentes.incidente_hijosubincidente 											as "KEY SUBTAREA",
		--incidentes.nombre_subtask																		as "DESC SUBTAREA",
		--incidentes.estado_sub_task																	as "ESTADO SUBTAREA",
		incidentes.usuario_jefe_de_fabrica, 
		incidentes.usuario_lider_de_fabrica, 
		incidentes.usuario_lider_proyecto,
		incidentes.comite_estimado,
		incidentes.funnel

FROM dwh_22.c_planificacion
									INNER JOIN dwh_22.d_assigneekey 								ON dwh_22.c_planificacion.assigneekey_id 		= dwh_22.d_assigneekey."id"
									INNER JOIN dwh_22.d_assigneeuser_key 						ON dwh_22.c_planificacion.assigneeuser_key_id 	= dwh_22.d_assigneeuser_key."id"
									INNER JOIN dwh_22.d_plan_itemkey 								ON dwh_22.c_planificacion.plan_itemkey_id 		= dwh_22.d_plan_itemkey."id"
									INNER JOIN dwh_22.d_plan_itemsummary 						ON dwh_22.c_planificacion.plan_itemsummary_id 	= dwh_22.d_plan_itemsummary."id"
									INNER JOIN dwh_22.d_start_time 									ON dwh_22.c_planificacion.start_time_id 			= dwh_22.d_start_time."id"
									INNER JOIN dwh_22.d_start 											ON dwh_22.c_planificacion.start_id 				= dwh_22.d_start."id"
									INNER JOIN dwh_22.d_end 												ON dwh_22.c_planificacion.end_id 				= dwh_22.d_end."id"
									INNER JOIN dwh_22.d_plan_itemissue_statusname		ON dwh_22.c_planificacion.plan_itemissue_statusname_id = d_plan_itemissue_statusname."id"
									LEFT JOIN  dwh_22.d_created_by 									ON dwh_22.c_planificacion.created_by_id 		= dwh_22.d_created_by."id"
									LEFT JOIN  dwh_22.d_created_by_key 							ON dwh_22.c_planificacion.created_by_key_id 	= dwh_22.d_created_by_key."id"
									LEFT JOIN 
											( 
											 SELECT
													dwh_22.d_membership_beanrolename.membership_beanrolename,
													dwh_22.c_miembros.membershipavailability,
													dwh_22.d_membership_beandate_from_ansi.membership_beandate_from_ansi,
													dwh_22.d_membership_beandate_to_ansi.membership_beandate_to_ansi,
													dwh_22.d_member_beandisplayname.member_beandisplayname,
													dwh_22.d_membership_beanstatus.membership_beanstatus,
													dwh_22.d_memberkey.memberkey,
													dwh_22.c_miembros.membership_beanteam,
													dwh_22.d_name."name" as "Nombre Departamento" 
													FROM
													dwh_22.c_miembros
													LEFT JOIN dwh_22.d_membership_beanrolename 				ON dwh_22.c_miembros.membershiprolename_id 									= dwh_22.d_membership_beanrolename."id"
													LEFT JOIN dwh_22.d_membership_beandate_from_ansi 	ON dwh_22.c_miembros.membership_beandate_from_ansi_id 			= dwh_22.d_membership_beandate_from_ansi."id"
													LEFT JOIN dwh_22.d_membership_beandate_to_ansi 		ON dwh_22.c_miembros.membership_beandate_to_ansi_id					= dwh_22.d_membership_beandate_to_ansi."id"
													LEFT JOIN dwh_22.d_member_beandisplayname 				ON dwh_22.c_miembros.member_beandisplayname_id 							= dwh_22.d_member_beandisplayname."id"
													LEFT JOIN dwh_22.d_membership_beanstatus 					ON dwh_22.c_miembros.membership_beanstatu_id 								= dwh_22.d_membership_beanstatus."id"
													LEFT JOIN dwh_22.d_memberkey 											ON dwh_22.c_miembros.memberkey_id 													= dwh_22.d_memberkey."id"
													INNER JOIN dwh_22.c_equipos 											ON dwh_22.c_miembros.membership_beanteam 										= dwh_22.c_equipos."id"
								INNER JOIN dwh_22.d_name 												ON dwh_22.c_equipos.name_id 													= dwh_22.d_name."id"
											) as team on dwh_22.d_assigneeuser_key.assigneeuser_key = team.memberkey
											LEFT JOIN 
											(
											SELECT
												dwh_22.d_issue_key.issue_key, 
												dwh_22.c_horasregistradas.hours											
											FROM dwh_22.c_horasregistradas
		INNER JOIN dwh_22.d_issue_key ON dwh_22.c_horasregistradas.issue_key_id = dwh_22.d_issue_key."id" where dwh_22.d_issue_key.issue_key like 'PMO-%' ORDER BY dwh_22.d_issue_key.issue_key desc
											) as worklog on dwh_22.d_plan_itemkey.plan_itemkey = worklog.issue_key
										INNER JOIN 
										(
											SELECT
													dwh_22.d_incidente_padre.incidente_padre,
													dwh_22.d_descripcion_origen.descripcion_origen,
													dwh_22.d_estado_incidente_padre.estado_incidente_padre,
													dwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica,
													dwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica,
													dwh_22.d_usuario_lider_proyecto.usuario_lider_proyecto,
													dwh_22.d_incidente_hijo.incidente_hijo,
													dwh_22.d_descripcion_destino.descripcion_destino,
													dwh_22.c_data_jira.planificacion_original,
													--dwh_22.d_estado_incidente_hijo.estado_incidente_hijo,
													--dwh_22.d_incidente_hijosubincidente.incidente_hijosubincidente,
													--dwh_22.d_nombre_subtask.nombre_subtask,
													--dwh_22.d_estado_sub_task.estado_sub_task,
													dwh_22.d_comite_estimado.comite_estimado,
													dwh_22.d_funnel.funnel
													FROM
													dwh_22.c_data_jira
													INNER JOIN dwh_22.d_descripcion_origen ON dwh_22.c_data_jira.descripcion_origen_id = dwh_22.d_descripcion_origen."id"
													INNER JOIN dwh_22.d_incidente_hijo ON dwh_22.c_data_jira.incidente_hijo_id = dwh_22.d_incidente_hijo."id"
													--INNER JOIN dwh_22.d_incidente_hijosubincidente ON dwh_22.c_data_jira.incidente_hijosubincidente_id = dwh_22.d_incidente_hijosubincidente."id"
													INNER JOIN dwh_22.d_descripcion_destino ON dwh_22.c_data_jira.descripcion_destino_id = dwh_22.d_descripcion_destino."id"
													--INNER JOIN dwh_22.d_nombre_subtask ON dwh_22.c_data_jira.nombre_subtask_id = dwh_22.d_nombre_subtask."id"
													INNER JOIN dwh_22.d_incidente_padre ON dwh_22.c_data_jira.incidente_padre_id = dwh_22.d_incidente_padre."id"
													INNER JOIN dwh_22.d_estado_incidente_padre ON dwh_22.c_data_jira.estado_incidente_padre_id = dwh_22.d_estado_incidente_padre."id"
													INNER JOIN dwh_22.d_estado_incidente_hijo ON dwh_22.c_data_jira.estado_incidente_hijo_id = dwh_22.d_estado_incidente_hijo."id"
													--INNER JOIN dwh_22.d_estado_sub_task ON dwh_22.c_data_jira.estado_sub_task_id = dwh_22.d_estado_sub_task."id"
													--INNER JOIN dwh_22.d_planificacion_original ON dwh_22.c_data_jira.planificacion_original_id = dwh_22.d_planificacion_original."id"
													LEFT JOIN dwh_22.d_usuario_jefe_de_fabrica ON dwh_22.c_data_jira.usuario_jefe_de_fabrica_id = dwh_22.d_usuario_jefe_de_fabrica."id"
													LEFT JOIN dwh_22.d_usuario_lider_de_fabrica ON dwh_22.c_data_jira.usuario_lider_de_fabrica_id = dwh_22.d_usuario_lider_de_fabrica."id"
													LEFT JOIN dwh_22.d_usuario_lider_proyecto ON dwh_22.c_data_jira.usuario_lider_proyecto_id = dwh_22.d_usuario_lider_proyecto."id"
													LEFT JOIN dwh_22.d_funnel ON dwh_22.c_data_jira.funnel_id = dwh_22.d_funnel."id"
													LEFT JOIN dwh_22.d_comite_estimado on dwh_22.c_data_jira.comite_estimado_id = dwh_22.d_comite_estimado."id"
																								) as incidentes on dwh_22.d_plan_itemkey.plan_itemkey = incidentes.incidente_hijo
											
 order by   dwh_22.c_planificacion."id" desc