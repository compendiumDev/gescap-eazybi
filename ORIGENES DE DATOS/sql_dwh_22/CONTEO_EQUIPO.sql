/*
Nombre CUBO : CONTEO_EQUIPO
Servidor : 152.139.147.122
puerto : 5432
base de datos : eazybi_qa
usuario : postgres
Cliente : BCH-GESCAP
*/
SELECT
TABLA_FINAL_PARA_CONTEO.incidente_padre
,COUNT (TABLA_FINAL_PARA_CONTEO.asignado_hijo)
,concat(TABLA_FINAL_PARA_CONTEO.MONTH_PLANIFICADO ,'-',TABLA_FINAL_PARA_CONTEO.YEAR_PLANIFICADO) AS FECHA_MES
--,TABLA_FINAL_PARA_CONTEO.summary
,TABLA_FINAL_PARA_CONTEO.name
,TABLA_FINAL_PARA_CONTEO.descripcion_origen
,TABLA_FINAL_PARA_CONTEO.estado_incidente_padre
,TABLA_FINAL_PARA_CONTEO.usuario_jefe_de_fabrica
,TABLA_FINAL_PARA_CONTEO.usuario_lider_proyecto
,TABLA_FINAL_PARA_CONTEO.usuario_lider_de_fabrica
,TABLA_FINAL_PARA_CONTEO.comite_estimado
FROM
(
select DISTINCT
dwh_22.d_incidente_padre.incidente_padre
,usuario_planificado.asignado_hijo
,usuario_planificado.MONTH_PLANIFICADO
,usuario_planificado.YEAR_PLANIFICADO
,dwh_22.d_summary.summary
,dwh_22.d_name.name
,dwh_22.d_lead.lead
,dwh_22.d_descripcion_origen.descripcion_origen
,dwh_22.d_estado_incidente_padre.estado_incidente_padre
,dwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica
,dwh_22.d_usuario_lider_proyecto.usuario_lider_proyecto
,dwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica
,dwh_22.d_comite_estimado.comite_estimado
from dwh_22.c_data_jira_1
left join dwh_22.c_equipos on dwh_22.c_data_jira_1.usuario_lider_de_fabrica_id = dwh_22.c_equipos.lead_id
left JOIN dwh_22.d_name ON 	c_equipos.name_id = d_name.id
left JOIN dwh_22.d_lead ON c_equipos.lead_id = d_lead.id
left JOIN dwh_22.d_summary ON c_equipos.summary_id = d_summary.id
left join dwh_22.d_incidente_padre on dwh_22.c_data_jira_1.incidente_padre_id = dwh_22.d_incidente_padre.id
left join dwh_22.d_estado_incidente_padre on dwh_22.c_data_jira_1.estado_incidente_padre_id = dwh_22.d_estado_incidente_padre.id
left join dwh_22.d_incidente_hijo on dwh_22.c_data_jira_1.incidente_hijo_id = dwh_22.d_incidente_hijo.id
left join dwh_22.d_descripcion_origen on dwh_22.c_data_jira_1.descripcion_origen_id = dwh_22.d_descripcion_origen.id
left join dwh_22.d_usuario_jefe_de_fabrica on dwh_22.c_data_jira_1.usuario_jefe_de_fabrica_id = dwh_22.d_usuario_jefe_de_fabrica.id
left join dwh_22.d_usuario_lider_proyecto on dwh_22.c_data_jira_1.usuario_lider_proyecto_id = dwh_22.d_usuario_lider_proyecto.id
left join dwh_22.d_usuario_lider_de_fabrica on dwh_22.c_data_jira_1.usuario_lider_de_fabrica_id = dwh_22.d_usuario_lider_de_fabrica.id
left join dwh_22.d_comite_estimado on dwh_22.c_data_jira_1.comite_estimado_id = dwh_22.d_comite_estimado.id
left join 
(
SELECT
serie_fecha_segundos_dia.issue_hijo
,serie_fecha_segundos_dia.asignado_hijo
,EXTRACT(MONTH FROM serie_fecha_segundos_dia.FECHA_DIA_PLANIFICADO) AS MONTH_PLANIFICADO
,EXTRACT(YEAR FROM serie_fecha_segundos_dia.FECHA_DIA_PLANIFICADO) AS YEAR_PLANIFICADO
,sum(serie_fecha_segundos_dia.seconds_per_day)/3600 AS HORAS_POR_MES
FROM
(
select DISTINCT
dwh_22.d_plan_itemkey.plan_itemkey AS issue_hijo
,dwh_22.d_assigneekey.assigneekey AS asignado_hijo
,dwh_22.c_planificacion.seconds_per_day
,dwh_22.d_start.month_name AS INICIO
,dwh_22.d_end.month_name AS FIN
,generate_series(dwh_22.d_start.time_value, dwh_22.d_end.time_value, '1 day') as FECHA_DIA_PLANIFICADO
from dwh_22.c_planificacion
left join dwh_22.d_plan_itemkey ON dwh_22.c_planificacion.plan_itemkey_id = dwh_22.d_plan_itemkey.id
left join dwh_22.d_assigneekey ON dwh_22.c_planificacion.assigneekey_id = dwh_22.d_assigneekey.id
left join dwh_22.d_assigneeuser_key ON dwh_22.c_planificacion.assigneeuser_key_id = dwh_22.d_assigneeuser_key.id
left join dwh_22.d_start on dwh_22.c_planificacion.start_id =dwh_22.d_start.id
left join dwh_22.d_end on dwh_22.c_planificacion.end_id =dwh_22.d_end.id
left join dwh_22.d_recurrence_enddate on dwh_22.c_planificacion.recurrence_enddate_id = dwh_22.d_recurrence_enddate.id
group by 1,2,3,4,5,6
) as serie_fecha_segundos_dia
GROUP BY 1,2,3,4
) AS usuario_planificado ON dwh_22.d_incidente_hijo.incidente_hijo = usuario_planificado.issue_hijo
) AS TABLA_FINAL_PARA_CONTEO
GROUP BY 1,3,4,5,6,7,8,9,10