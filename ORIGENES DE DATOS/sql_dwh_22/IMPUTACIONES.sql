/*
Nombre CUBO : IMPUTACIONES
Servidor : 152.139.147.122
puerto : 5432
base de datos : eazybi_qa
usuario : postgres
Cliente : BCH-GESCAP
*/
SELECT
		dwh_22.d_incidente_padre.incidente_padre
	,	dwh_22.d_descripcion_origen.descripcion_origen
	,	dwh_22.d_estado_incidente_padre.estado_incidente_padre
	,	dwh_22.d_incidente_hijo.incidente_hijo
	,	dwh_22.d_descripcion_destino.descripcion_destino
	,	dwh_22.d_estado_incidente_hijo.estado_incidente_hijo
	,	dwh_22.d_id_subtarea.id_subtarea
	,	dwh_22.d_name.name
	,	dwh_22.c_data_jira_1.planificacion_original
	,	dwh_22.d_nombre_subtask.nombre_subtask
	,	dwh_22.d_estado_sub_task.estado_sub_task
	,	dwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica
	,	dwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica usuario_lider_de_fabric
	,	dwh_22.d_usuario_lider_proyecto.usuario_lider_proyecto
	,	dwh_22.d_funnel.funnel
	,	dwh_22.d_comite_estimado.comite_estimado
	,   registroHoras.username as Usuario_asignado
	,   registroHoras.UsuarioAsignadoSistema
	,	registroHoras.fechaRegistroHoras
	,   registroHoras.horas
FROM
dwh_22.c_data_jira_1
	LEFT JOIN dwh_22.c_equipos 												ON dwh_22.c_data_jira_1.usuario_lider_de_fabrica_id 	= dwh_22.c_equipos.lead_id
	LEFT JOIN dwh_22.d_name 													ON dwh_22.c_equipos.name_id 													= dwh_22.d_name.id
	LEFT JOIN dwh_22.d_link_origen 										ON dwh_22.c_data_jira_1.link_origen_id 								= dwh_22.d_link_origen."id"
	LEFT JOIN dwh_22.d_incidente_padre 								ON dwh_22.c_data_jira_1.incidente_padre_id 						= dwh_22.d_incidente_padre."id"
	LEFT JOIN dwh_22.d_incidente_hijo 								ON dwh_22.c_data_jira_1.incidente_hijo_id 						= dwh_22.d_incidente_hijo."id"
	LEFT JOIN dwh_22.d_incidente_hijosubincidente 		ON dwh_22.c_data_jira_1.incidente_hijosubincidente_id = dwh_22.d_incidente_hijosubincidente."id"
	LEFT JOIN dwh_22.d_id_subtarea 										ON dwh_22.c_data_jira_1.id_subtarea_id 								= dwh_22.d_id_subtarea."id"
	LEFT JOIN dwh_22.d_descripcion_origen 						ON dwh_22.c_data_jira_1.descripcion_origen_id 				= dwh_22.d_descripcion_origen."id"
	LEFT JOIN dwh_22.d_descripcion_destino 						ON dwh_22.c_data_jira_1.descripcion_destino_id 				= dwh_22.d_descripcion_destino."id"
	LEFT JOIN dwh_22.d_nombre_subtask 								ON dwh_22.c_data_jira_1.nombre_subtask_id 						= dwh_22.d_nombre_subtask."id"
	LEFT JOIN dwh_22.d_estado_incidente_padre 				ON dwh_22.c_data_jira_1.estado_incidente_padre_id 		= dwh_22.d_estado_incidente_padre."id"
	LEFT JOIN dwh_22.d_estado_incidente_hijo 					ON dwh_22.c_data_jira_1.estado_incidente_hijo_id 			= dwh_22.d_estado_incidente_hijo."id"
	LEFT JOIN dwh_22.d_estado_sub_task 								ON dwh_22.c_data_jira_1.estado_sub_task_id 						= dwh_22.d_estado_sub_task."id"
	LEFT JOIN dwh_22.d_usuario_jefe_de_fabrica 				ON dwh_22.c_data_jira_1.usuario_jefe_de_fabrica_id 		= dwh_22.d_usuario_jefe_de_fabrica."id"
	LEFT JOIN dwh_22.d_usuario_lider_de_fabrica 			ON dwh_22.c_data_jira_1.usuario_lider_de_fabrica_id 	= dwh_22.d_usuario_lider_de_fabrica."id"
	LEFT JOIN dwh_22.d_usuario_lider_proyecto 				ON dwh_22.c_data_jira_1.usuario_lider_proyecto_id 		= dwh_22.d_usuario_lider_proyecto."id"
	LEFT JOIN dwh_22.d_funnel 												ON dwh_22.c_data_jira_1.funnel_id 										= dwh_22.d_funnel."id"
	LEFT JOIN dwh_22.d_comite_estimado 								ON dwh_22.c_data_jira_1.comite_estimado_id 						= dwh_22.d_comite_estimado."id"
LEFT JOIN (
		select 	
						max(logeo.worklog) id 
					, logeo.username
					, dataplan.UsuarioAsignadoSistema
					, max(logeo.fecha) fechaRegistroHoras
					,	logeo.issue_key
					, sum(logeo.horas) horas from 
					(
					SELECT DISTINCT ON (dwh_22.c_horasregistradas.worklog)
						dwh_22.c_horasregistradas.worklog,
						dwh_22.d_staff.staff,
						dwh_22.d_username.username,
						dwh_22.d_time.time_value as fecha,
						dwh_22.d_issue_key.issue_key,
						SUM ( dwh_22.c_horasregistradas.hours ) AS horas 
					FROM
						dwh_22.c_horasregistradas
						INNER JOIN dwh_22.d_staff 			ON dwh_22.c_horasregistradas.staff_id 			= dwh_22.d_staff."id"
						INNER JOIN dwh_22.d_username 		ON dwh_22.c_horasregistradas.username_id 		= dwh_22.d_username."id"
						INNER JOIN dwh_22.d_time 				ON dwh_22.c_horasregistradas.time_id 			= dwh_22.d_time."id"
						INNER JOIN dwh_22.d_issue_key 	ON dwh_22.c_horasregistradas.issue_key_id 		= dwh_22.d_issue_key."id" 
					WHERE
						dwh_22.d_issue_key.issue_key LIKE 'PMO-%' --and d_issue_key.issue_key = 'PMO-9417'
						group by
						dwh_22.c_horasregistradas.worklog,
						dwh_22.d_staff.staff, 
						dwh_22.d_username.username, 
						dwh_22.d_time.time_value, 
						dwh_22.d_issue_key.issue_key
						) logeo 
						LEFT JOIN 
							(
							SELECT
						dwh_22.d_assigneekey.assigneekey 													AS UsuarioAsignado,
						dwh_22.d_assigneeuser_key.assigneeuser_key 								AS UsuarioAsignadoSistema
					FROM
						dwh_22.c_planificacion
						INNER JOIN dwh_22.d_assigneekey ON dwh_22.c_planificacion.assigneekey_id = dwh_22.d_assigneekey.id
						INNER JOIN dwh_22.d_assigneeuser_key ON dwh_22.c_planificacion.assigneeuser_key_id = dwh_22.d_assigneeuser_key.id
						INNER JOIN dwh_22.d_plan_itemkey ON dwh_22.c_planificacion.plan_itemkey_id = dwh_22.d_plan_itemkey.id
						INNER JOIN dwh_22.d_plan_itemsummary ON dwh_22.c_planificacion.plan_itemsummary_id = dwh_22.d_plan_itemsummary.id
						INNER JOIN dwh_22.d_start_time ON dwh_22.c_planificacion.start_time_id = dwh_22.d_start_time.id
						INNER JOIN dwh_22.d_start ON dwh_22.c_planificacion.start_id = dwh_22.d_start.id
						INNER JOIN dwh_22.d_end ON dwh_22.c_planificacion.end_id = dwh_22.d_end.id
						INNER JOIN dwh_22.d_plan_itemissue_statusname ON dwh_22.c_planificacion.plan_itemissue_statusname_id = d_plan_itemissue_statusname.id
						INNER JOIN dwh_22.d_created_by ON dwh_22.c_planificacion.created_by_id = dwh_22.d_created_by.id
						INNER JOIN dwh_22.d_created_by_key ON dwh_22.c_planificacion.created_by_key_id = dwh_22.d_created_by_key.id 
					WHERE
						d_plan_itemkey.plan_itemkey LIKE 'PMO-%'						
					group by dwh_22.d_assigneekey.assigneekey,
						dwh_22.d_assigneeuser_key.assigneeuser_key
							) as dataplan on logeo.username = dataplan.UsuarioAsignado	
						--WHERE logeo.issue_key = 'PMO-10302'
						GROUP BY 	logeo.username
										,	dataplan.UsuarioAsignadoSistema
										--,	logeo.fecha
										, logeo.issue_key
						--ORDER BY logeo.fecha
	) registroHoras on d_id_subtarea.id_subtarea = registroHoras.issue_key
	order by 1,4/*
Nombre CUBO : IMPUTACIONES
Servidor : 152.139.147.122
puerto : 5432
base de datos : eazybi_qa
usuario : postgres
Cliente : BCH-GESCAP
*/
SELECT
		dwh_22.d_incidente_padre.incidente_padre
	,	dwh_22.d_descripcion_origen.descripcion_origen
	,	dwh_22.d_estado_incidente_padre.estado_incidente_padre
	,	dwh_22.d_incidente_hijo.incidente_hijo
	,	dwh_22.d_descripcion_destino.descripcion_destino
	,	dwh_22.d_estado_incidente_hijo.estado_incidente_hijo
	,	dwh_22.d_id_subtarea.id_subtarea
	,	dwh_22.d_name.name
	,	dwh_22.c_data_jira_1.planificacion_original
	,	dwh_22.d_nombre_subtask.nombre_subtask
	,	dwh_22.d_estado_sub_task.estado_sub_task
	,	dwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica
	,	dwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica usuario_lider_de_fabric
	,	dwh_22.d_usuario_lider_proyecto.usuario_lider_proyecto
	,	dwh_22.d_funnel.funnel
	,	dwh_22.d_comite_estimado.comite_estimado
	,   registroHoras.username as Usuario_asignado
	,   registroHoras.UsuarioAsignadoSistema
	,	registroHoras.fechaRegistroHoras
	,   registroHoras.horas
FROM
dwh_22.c_data_jira_1
	LEFT JOIN dwh_22.c_equipos 												ON dwh_22.c_data_jira_1.usuario_lider_de_fabrica_id 	= dwh_22.c_equipos.lead_id
	LEFT JOIN dwh_22.d_name 													ON dwh_22.c_equipos.name_id 													= dwh_22.d_name.id
	LEFT JOIN dwh_22.d_link_origen 										ON dwh_22.c_data_jira_1.link_origen_id 								= dwh_22.d_link_origen."id"
	LEFT JOIN dwh_22.d_incidente_padre 								ON dwh_22.c_data_jira_1.incidente_padre_id 						= dwh_22.d_incidente_padre."id"
	LEFT JOIN dwh_22.d_incidente_hijo 								ON dwh_22.c_data_jira_1.incidente_hijo_id 						= dwh_22.d_incidente_hijo."id"
	LEFT JOIN dwh_22.d_incidente_hijosubincidente 		ON dwh_22.c_data_jira_1.incidente_hijosubincidente_id = dwh_22.d_incidente_hijosubincidente."id"
	LEFT JOIN dwh_22.d_id_subtarea 										ON dwh_22.c_data_jira_1.id_subtarea_id 								= dwh_22.d_id_subtarea."id"
	LEFT JOIN dwh_22.d_descripcion_origen 						ON dwh_22.c_data_jira_1.descripcion_origen_id 				= dwh_22.d_descripcion_origen."id"
	LEFT JOIN dwh_22.d_descripcion_destino 						ON dwh_22.c_data_jira_1.descripcion_destino_id 				= dwh_22.d_descripcion_destino."id"
	LEFT JOIN dwh_22.d_nombre_subtask 								ON dwh_22.c_data_jira_1.nombre_subtask_id 						= dwh_22.d_nombre_subtask."id"
	LEFT JOIN dwh_22.d_estado_incidente_padre 				ON dwh_22.c_data_jira_1.estado_incidente_padre_id 		= dwh_22.d_estado_incidente_padre."id"
	LEFT JOIN dwh_22.d_estado_incidente_hijo 					ON dwh_22.c_data_jira_1.estado_incidente_hijo_id 			= dwh_22.d_estado_incidente_hijo."id"
	LEFT JOIN dwh_22.d_estado_sub_task 								ON dwh_22.c_data_jira_1.estado_sub_task_id 						= dwh_22.d_estado_sub_task."id"
	LEFT JOIN dwh_22.d_usuario_jefe_de_fabrica 				ON dwh_22.c_data_jira_1.usuario_jefe_de_fabrica_id 		= dwh_22.d_usuario_jefe_de_fabrica."id"
	LEFT JOIN dwh_22.d_usuario_lider_de_fabrica 			ON dwh_22.c_data_jira_1.usuario_lider_de_fabrica_id 	= dwh_22.d_usuario_lider_de_fabrica."id"
	LEFT JOIN dwh_22.d_usuario_lider_proyecto 				ON dwh_22.c_data_jira_1.usuario_lider_proyecto_id 		= dwh_22.d_usuario_lider_proyecto."id"
	LEFT JOIN dwh_22.d_funnel 												ON dwh_22.c_data_jira_1.funnel_id 										= dwh_22.d_funnel."id"
	LEFT JOIN dwh_22.d_comite_estimado 								ON dwh_22.c_data_jira_1.comite_estimado_id 						= dwh_22.d_comite_estimado."id"
LEFT JOIN (
		select 	
						max(logeo.worklog) id 
					, logeo.username
					, dataplan.UsuarioAsignadoSistema
					, max(logeo.fecha) fechaRegistroHoras
					,	logeo.issue_key
					, sum(logeo.horas) horas from 
					(
					SELECT DISTINCT ON (dwh_22.c_horasregistradas.worklog)
						dwh_22.c_horasregistradas.worklog,
						dwh_22.d_staff.staff,
						dwh_22.d_username.username,
						dwh_22.d_time.time_value as fecha,
						dwh_22.d_issue_key.issue_key,
						SUM ( dwh_22.c_horasregistradas.hours ) AS horas 
					FROM
						dwh_22.c_horasregistradas
						INNER JOIN dwh_22.d_staff 			ON dwh_22.c_horasregistradas.staff_id 			= dwh_22.d_staff."id"
						INNER JOIN dwh_22.d_username 		ON dwh_22.c_horasregistradas.username_id 		= dwh_22.d_username."id"
						INNER JOIN dwh_22.d_time 				ON dwh_22.c_horasregistradas.time_id 			= dwh_22.d_time."id"
						INNER JOIN dwh_22.d_issue_key 	ON dwh_22.c_horasregistradas.issue_key_id 		= dwh_22.d_issue_key."id" 
					WHERE
						dwh_22.d_issue_key.issue_key LIKE 'PMO-%' --and d_issue_key.issue_key = 'PMO-9417'
						group by
						dwh_22.c_horasregistradas.worklog,
						dwh_22.d_staff.staff, 
						dwh_22.d_username.username, 
						dwh_22.d_time.time_value, 
						dwh_22.d_issue_key.issue_key
						) logeo 
						LEFT JOIN 
							(
							SELECT
						dwh_22.d_assigneekey.assigneekey 													AS UsuarioAsignado,
						dwh_22.d_assigneeuser_key.assigneeuser_key 								AS UsuarioAsignadoSistema
					FROM
						dwh_22.c_planificacion
						INNER JOIN dwh_22.d_assigneekey ON dwh_22.c_planificacion.assigneekey_id = dwh_22.d_assigneekey.id
						INNER JOIN dwh_22.d_assigneeuser_key ON dwh_22.c_planificacion.assigneeuser_key_id = dwh_22.d_assigneeuser_key.id
						INNER JOIN dwh_22.d_plan_itemkey ON dwh_22.c_planificacion.plan_itemkey_id = dwh_22.d_plan_itemkey.id
						INNER JOIN dwh_22.d_plan_itemsummary ON dwh_22.c_planificacion.plan_itemsummary_id = dwh_22.d_plan_itemsummary.id
						INNER JOIN dwh_22.d_start_time ON dwh_22.c_planificacion.start_time_id = dwh_22.d_start_time.id
						INNER JOIN dwh_22.d_start ON dwh_22.c_planificacion.start_id = dwh_22.d_start.id
						INNER JOIN dwh_22.d_end ON dwh_22.c_planificacion.end_id = dwh_22.d_end.id
						INNER JOIN dwh_22.d_plan_itemissue_statusname ON dwh_22.c_planificacion.plan_itemissue_statusname_id = d_plan_itemissue_statusname.id
						INNER JOIN dwh_22.d_created_by ON dwh_22.c_planificacion.created_by_id = dwh_22.d_created_by.id
						INNER JOIN dwh_22.d_created_by_key ON dwh_22.c_planificacion.created_by_key_id = dwh_22.d_created_by_key.id 
					WHERE
						d_plan_itemkey.plan_itemkey LIKE 'PMO-%'						
					group by dwh_22.d_assigneekey.assigneekey,
						dwh_22.d_assigneeuser_key.assigneeuser_key
							) as dataplan on logeo.username = dataplan.UsuarioAsignado	
						--WHERE logeo.issue_key = 'PMO-10302'
						GROUP BY 	logeo.username
										,	dataplan.UsuarioAsignadoSistema
										--,	logeo.fecha
										, logeo.issue_key
						--ORDER BY logeo.fecha
	) registroHoras on d_id_subtarea.id_subtarea = registroHoras.issue_key
	order by 1,4