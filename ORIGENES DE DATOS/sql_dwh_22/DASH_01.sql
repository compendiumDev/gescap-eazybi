/*
Nombre CUBO : DASH_01
Servidor : 152.139.147.122
puerto : 5432
base de datos : eazybi_qa
usuario : postgres
Cliente : BCH-GESCAP
*/

select
plantotalhoras.ID,
plantotalhoras.Departamento_,
plantotalhoras.Usuario_Jira,
plantotalhoras.Nombre_Completo,
plantotalhoras.Porcentaje_Disponibilidad_diaria,
plantotalhoras.Rol,
case when plantotalhoras.diaplanificado IS NULL THEN '1900-01-01 00:00:00' ELSE plantotalhoras.diaplanificado END d_plan,
case when plantotalhoras.dw IS NULL THEN 0 ELSE plantotalhoras.dw END,
plantotalhoras.cantUsuarioEquipo,
plantotalhoras.horasTotalesDiariasEquipo,
plantotalhoras.FechaInicioPlan,
plantotalhoras.FechaTerminoPlan,
plantotalhoras.HorasPlanificadosPorDia,
round(cast (plantotalhoras.planificacion_original as decimal) / 3600 / cast (plantotalhoras.HorasPlanificadosPorDia as decimal),0) planificacion_original,
plantotalhoras.codigo,
plantotalhoras.Nombre,
plantotalhoras.UsuarioAsignado,
plantotalhoras.UsuarioAsignadoSistema,
plantotalhoras.EstadoIssue,
plantotalhoras.PlanificacionCreadaPor_USER,
plantotalhoras.PlanificacionCreadaPor_KEY,
plantotalhoras.incidente_padre,
plantotalhoras.descripcion_origen,
plantotalhoras.estado_incidente_padre,
plantotalhoras.usuario_jefe_de_fabrica,
plantotalhoras.usuario_lider_de_fabrica,
plantotalhoras.usuario_lider_proyecto,
plantotalhoras.incidente_hijo,
plantotalhoras.descripcion_destino,
plantotalhoras.comite_estimado,
plantotalhoras.funnel
FROM
(
SELECT ROW_NUMBER
( ) OVER ( ORDER BY horastotalesequipos.diaPlanificado ) AS ID,
( CASE WHEN plantotal.Departamento IS NULL THEN '-' ELSE plantotal.Departamento END ) Departamento_,
( CASE WHEN plantotal.UsuarioJira IS NULL THEN '-' ELSE plantotal.UsuarioJira END ) Usuario_Jira,
( CASE WHEN plantotal.Nombre_Completo IS NULL THEN '-' ELSE plantotal.Nombre_Completo END ) Nombre_Completo,
( CASE WHEN plantotal.Porcentaje_Disponibilidad_diaria IS NULL THEN 100 ELSE plantotal.Porcentaje_Disponibilidad_diaria END ) Porcentaje_Disponibilidad_diaria,
( CASE WHEN plantotal.Rol IS NULL THEN '-' ELSE plantotal.Rol END ) Rol,
( CASE WHEN horastotalesequipos.diaplanificado IS NULL THEN horastotalesequipos.diaplanificado ELSE plantotal.diaplanificado END ) diaplanificado,
( CASE WHEN horastotalesequipos.dw IS NULL THEN 0 ELSE plantotal.dw END ) dw,
( CASE WHEN horastotalesequipos.cantUsuarioEquipo IS NULL THEN 0 ELSE horastotalesequipos.cantUsuarioEquipo end) cantUsuarioEquipo,
( CASE WHEN horastotalesequipos.horasTotalesDiariasEquipo IS NULL THEN 0 ELSE horastotalesequipos.horasTotalesDiariasEquipo END ) horasTotalesDiariasEquipo,
( CASE WHEN plantotal.FechaInicioPlan IS NULL THEN horastotalesequipos.diaplanificado ELSE plantotal.FechaInicioPlan END ) AS FechaInicioPlan,
( CASE WHEN plantotal.FechaTerminoPlan IS NULL THEN horastotalesequipos.diaplanificado ELSE plantotal.FechaTerminoPlan END ) AS FechaTerminoPlan,
( CASE WHEN plantotal.HorasPlanificadosPorDia IS NULL THEN 1 WHEN plantotal.HorasPlanificadosPorDia = 0 THEN 1 ELSE plantotal.HorasPlanificadosPorDia END ) HorasPlanificadosPorDia,
--plantotal.HorasPlanificadosPorDia AS HorasPlanificadosPorDia,
( CASE WHEN plantotal.planificacion_original IS NULL THEN '0' ELSE plantotal.planificacion_original END ) planificacion_original,
( CASE WHEN plantotal.codigo IS NULL THEN '-' ELSE plantotal.codigo END ) codigo,
( CASE WHEN plantotal.Nombre IS NULL THEN '-' ELSE plantotal.Nombre END ) Nombre,
( CASE WHEN plantotal.UsuarioAsignado IS NULL THEN '-' ELSE plantotal.UsuarioAsignado END ) UsuarioAsignado,
( CASE WHEN plantotal.UsuarioAsignadoSistema IS NULL THEN '-' ELSE plantotal.UsuarioAsignadoSistema END ) UsuarioAsignadoSistema,
( CASE WHEN plantotal.EstadoIssue IS NULL THEN '-' ELSE plantotal.EstadoIssue END ) EstadoIssue,
( CASE WHEN plantotal.PlanificacionCreadaPor_USER IS NULL THEN '-' ELSE plantotal.PlanificacionCreadaPor_USER END ) PlanificacionCreadaPor_USER,
( CASE WHEN plantotal.PlanificacionCreadaPor_KEY IS NULL THEN '-' ELSE plantotal.PlanificacionCreadaPor_KEY END ) PlanificacionCreadaPor_KEY,
( CASE WHEN plantotal.incidente_padre IS NULL THEN '-' ELSE plantotal.incidente_padre END ) incidente_padre,
( CASE WHEN plantotal.descripcion_origen IS NULL THEN '-' ELSE plantotal.descripcion_origen END ) descripcion_origen,
( CASE WHEN plantotal.estado_incidente_padre IS NULL THEN '-' ELSE plantotal.estado_incidente_padre END ) estado_incidente_padre,
( CASE WHEN plantotal.usuario_jefe_de_fabrica IS NULL THEN '-' ELSE plantotal.usuario_jefe_de_fabrica END ) usuario_jefe_de_fabrica,
( CASE WHEN plantotal.usuario_lider_de_fabrica IS NULL THEN '-' ELSE plantotal.usuario_lider_de_fabrica END ) usuario_lider_de_fabrica,
( CASE WHEN plantotal.usuario_lider_proyecto IS NULL THEN '-' ELSE plantotal.usuario_lider_proyecto END ) usuario_lider_proyecto,
( CASE WHEN plantotal.incidente_hijo IS NULL THEN '-' ELSE plantotal.incidente_hijo END ) incidente_hijo,
( CASE WHEN plantotal.descripcion_destino IS NULL THEN '-' ELSE plantotal.descripcion_destino END ) descripcion_destino,
( CASE WHEN plantotal.comite_estimado IS NULL THEN '-' ELSE plantotal.comite_estimado END ) comite_estimado,
( CASE WHEN plantotal.funnel IS NULL THEN '-' ELSE plantotal.funnel END ) funnel 
FROM
(
SELECT
equipocap.NAME,
COUNT ( DISTINCT ( equipocap.idUsuario ) ) AS cantUsuarioEquipo,
COUNT ( DISTINCT ( equipocap.idUsuario ) ) * 8 AS horasTotalesDiariasEquipo,
equipocap.diaPlanificado,
equipocap.dw 
FROM
(
SELECT
dwh_22.c_equipos.ID AS idEquipo,
dwh_22.d_membername.ID AS idUsuario,
dwh_22.d_name.NAME,
dwh_22.d_membername.membername,
dwh_22.d_memberdisplayname.memberdisplayname,
dwh_22.d_lead.LEAD,
dwh_22.c_miembros.membershipavailability,
dwh_22.d_usuario_asignado.usuario_asignado,
dwh_22.c_base2.horas_planificadas_por_dia,
dwh_22.c_base2.planificacion_original,
dwh_22.c_base2.fecha_inicio_plan_id,
dwh_22.c_base2.fecha_termino_plan_id,
dwh_22.d_time.time_value,
generate_series ( MIN ( '2020-01-01' :: TIMESTAMP ), MAX ( '2022-01-01' :: TIMESTAMP ), INTERVAL '24 hour' ) AS diaPlanificado,
EXTRACT ( DOW FROM generate_series ( MIN ( '2020-01-01' :: TIMESTAMP ), MAX ( '2022-01-01' :: TIMESTAMP ), INTERVAL '24 hour' ) ) AS dw 
FROM
dwh_22.c_miembros
INNER JOIN dwh_22.c_equipos ON dwh_22.c_miembros.membership_beanteam :: INTEGER = dwh_22.c_equipos.ID 
INNER JOIN dwh_22.d_name ON dwh_22.c_equipos.name_id = dwh_22.d_name.ID 
INNER JOIN dwh_22.d_membername ON dwh_22.c_miembros.membername_id = dwh_22.d_membername.ID 
INNER JOIN dwh_22.d_memberdisplayname ON dwh_22.c_miembros.memberdisplayname_id = dwh_22.d_memberdisplayname.ID 
INNER JOIN dwh_22.d_lead ON dwh_22.c_equipos.lead_id = dwh_22.d_lead.ID 
LEFT JOIN dwh_22.d_usuario_asignado ON dwh_22.d_membername.membername = dwh_22.d_usuario_asignado.usuario_asignado
LEFT JOIN dwh_22.c_base2 ON dwh_22.d_usuario_asignado.ID = dwh_22.c_base2.usuario_asignado_id
LEFT JOIN dwh_22.d_time ON dwh_22.c_base2.d_plan_id = dwh_22.d_time.ID 
GROUP BY
dwh_22.c_equipos.ID,
dwh_22.d_membername.ID,
dwh_22.d_name.NAME,
dwh_22.d_membername.membername,
dwh_22.d_memberdisplayname.memberdisplayname,
dwh_22.d_lead.LEAD,
dwh_22.c_miembros.membershipavailability,
dwh_22.d_usuario_asignado.usuario_asignado,
dwh_22.c_base2.horas_planificadas_por_dia,
dwh_22.c_base2.planificacion_original,
dwh_22.c_base2.fecha_inicio_plan_id,
dwh_22.c_base2.fecha_termino_plan_id,
dwh_22.d_time.time_value 
) AS equipocap 
GROUP BY
equipocap.NAME,
equipocap.diaPlanificado,
equipocap.dw 
ORDER BY
equipocap.diaPlanificado ASC 
) AS horastotalesequipos
LEFT JOIN 
(
SELECT
dwh_22.d_name.NAME AS Departamento,
dwh_22.d_membername.membername AS UsuarioJira,
dwh_22.d_memberdisplayname.memberdisplayname AS Nombre_Completo,
dwh_22.c_miembros.membershipavailability AS Porcentaje_Disponibilidad_diaria,
dwh_22.d_membership_beanrolename.membership_beanrolename AS Rol,
generate_series ( MIN ( planificacion.FechaInicioPlan ), MAX ( planificacion.FechaTerminoPlan ), INTERVAL '24 hour' ) AS diaPlanificado,
EXTRACT ( DOW FROM generate_series ( MIN ( planificacion.FechaInicioPlan ), MAX ( planificacion.FechaTerminoPlan ), INTERVAL '24 hour' ) ) AS dw,
( ( 8 * dwh_22.c_miembros.membershipavailability ) / 100 ) HorasDisponiblesDia,
planificacion.FechaInicioPlan,
planificacion.FechaTerminoPlan,
planificacion.HorasPlanificadosPorDia,
jira.planificacion_original,
planificacion.codigo,
planificacion.Nombre,
planificacion.UsuarioAsignado,
planificacion.UsuarioAsignadoSistema,
planificacion.EstadoIssue,
planificacion.PlanificacionCreadaPor_USER,
planificacion.PlanificacionCreadaPor_KEY,
jira.incidente_padre,
jira.descripcion_origen,
jira.estado_incidente_padre,
jira.usuario_jefe_de_fabrica,
jira.usuario_lider_de_fabrica,
jira.usuario_lider_proyecto,
jira.incidente_hijo,
jira.descripcion_destino,
jira.comite_estimado,
jira.funnel 
FROM
dwh_22.c_miembros
INNER JOIN dwh_22.c_equipos ON dwh_22.c_miembros.membership_beanteam = c_equipos.
ID INNER JOIN dwh_22.d_name ON dwh_22.c_equipos.name_id = d_name.
ID INNER JOIN dwh_22.d_membername ON dwh_22.c_miembros.membername_id = d_membername.
ID INNER JOIN dwh_22.d_memberdisplayname ON dwh_22.c_miembros.memberdisplayname_id = dwh_22.d_memberdisplayname.
ID INNER JOIN dwh_22.d_membership_beanrolename ON dwh_22.c_miembros.membership_beanrolename_id = dwh_22.d_membership_beanrolename.
ID INNER JOIN (
SELECT
dwh_22.c_planificacion.ID,
dwh_22.d_plan_itemkey.plan_itemkey AS codigo,
dwh_22.d_plan_itemsummary.plan_itemsummary AS Nombre,
dwh_22.d_assigneekey.assigneekey AS UsuarioAsignado,
dwh_22.d_assigneeuser_key.assigneeuser_key AS UsuarioAsignadoSistema,
dwh_22.d_plan_itemissue_statusname.plan_itemissue_statusname AS EstadoIssue,
dwh_22.d_start.time_value AS FechaInicioPlan,
dwh_22.d_end.time_value AS FechaTerminoPlan,
( dwh_22.c_planificacion.seconds_per_day / 3600 ) AS HorasPlanificadosPorDia,
( dwh_22.c_planificacion.seconds / 3600 ) AS HorasTotalesPlanificados,
( dwh_22.c_planificacion.plan_itemestimated_remaining_seconds / 3600 ) AS HorasDisponiblesParaEstimacion,
dwh_22.d_created_by.created_by AS PlanificacionCreadaPor_USER,
dwh_22.d_created_by_key.created_by_key AS PlanificacionCreadaPor_KEY 
FROM
dwh_22.c_planificacion
INNER JOIN dwh_22.d_assigneekey ON dwh_22.c_planificacion.assigneekey_id = dwh_22.d_assigneekey.
ID INNER JOIN dwh_22.d_assigneeuser_key ON dwh_22.c_planificacion.assigneeuser_key_id = dwh_22.d_assigneeuser_key.
ID INNER JOIN dwh_22.d_plan_itemkey ON dwh_22.c_planificacion.plan_itemkey_id = dwh_22.d_plan_itemkey.
ID INNER JOIN dwh_22.d_plan_itemsummary ON dwh_22.c_planificacion.plan_itemsummary_id = dwh_22.d_plan_itemsummary.
ID INNER JOIN dwh_22.d_start_time ON dwh_22.c_planificacion.start_time_id = dwh_22.d_start_time.
ID INNER JOIN dwh_22.d_start ON dwh_22.c_planificacion.start_id = dwh_22.d_start.
ID INNER JOIN dwh_22.d_end ON dwh_22.c_planificacion.end_id = dwh_22.d_end.
ID INNER JOIN dwh_22.d_plan_itemissue_statusname ON dwh_22.c_planificacion.plan_itemissue_statusname_id = d_plan_itemissue_statusname.
ID INNER JOIN dwh_22.d_created_by ON dwh_22.c_planificacion.created_by_id = dwh_22.d_created_by.
ID INNER JOIN dwh_22.d_created_by_key ON dwh_22.c_planificacion.created_by_key_id = dwh_22.d_created_by_key.ID 
WHERE
d_plan_itemkey.plan_itemkey LIKE'PMO-%' 
) AS planificacion ON dwh_22.d_membername.membername = planificacion.UsuarioAsignado
LEFT JOIN (
SELECT
dwh_22.d_incidente_padre.incidente_padre,
dwh_22.d_descripcion_origen.descripcion_origen,
dwh_22.d_estado_incidente_padre.estado_incidente_padre,
dwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica,
dwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica,
dwh_22.d_usuario_lider_proyecto.usuario_lider_proyecto,
dwh_22.d_incidente_hijo.incidente_hijo,
dwh_22.d_descripcion_destino.descripcion_destino,
dwh_22.c_data_jira.planificacion_original,
dwh_22.d_comite_estimado.comite_estimado,
dwh_22.d_funnel.funnel 
FROM
dwh_22.c_data_jira
LEFT JOIN dwh_22.d_descripcion_origen ON dwh_22.c_data_jira.descripcion_origen_id = dwh_22.d_descripcion_origen.ID 
LEFT JOIN dwh_22.d_incidente_hijo ON dwh_22.c_data_jira.incidente_hijo_id = dwh_22.d_incidente_hijo.ID 
LEFT JOIN dwh_22.d_descripcion_destino ON dwh_22.c_data_jira.descripcion_destino_id = dwh_22.d_descripcion_destino.ID 
LEFT JOIN dwh_22.d_incidente_padre ON dwh_22.c_data_jira.incidente_padre_id = dwh_22.d_incidente_padre.ID 
LEFT JOIN dwh_22.d_estado_incidente_padre ON dwh_22.c_data_jira.estado_incidente_padre_id = dwh_22.d_estado_incidente_padre.ID 
LEFT JOIN dwh_22.d_estado_incidente_hijo ON dwh_22.c_data_jira.estado_incidente_hijo_id = dwh_22.d_estado_incidente_hijo.ID 
--LEFT JOIN dwh_22.d_planificacion_original ON dwh_22.c_data_jira.planificacion_original_id = dwh_22.d_planificacion_original.ID 
LEFT JOIN dwh_22.d_usuario_jefe_de_fabrica ON dwh_22.c_data_jira.usuario_jefe_de_fabrica_id = dwh_22.d_usuario_jefe_de_fabrica.ID 
LEFT JOIN dwh_22.d_usuario_lider_de_fabrica ON dwh_22.c_data_jira.usuario_lider_de_fabrica_id = dwh_22.d_usuario_lider_de_fabrica.ID 
LEFT JOIN dwh_22.d_usuario_lider_proyecto ON dwh_22.c_data_jira.usuario_lider_proyecto_id = dwh_22.d_usuario_lider_proyecto.ID 
LEFT JOIN dwh_22.d_funnel ON dwh_22.c_data_jira.funnel_id = dwh_22.d_funnel.ID 
LEFT JOIN dwh_22.d_comite_estimado ON dwh_22.c_data_jira.comite_estimado_id = dwh_22.d_comite_estimado.ID 
) AS jira ON planificacion.codigo = jira.incidente_hijo 
GROUP BY
dwh_22.d_name.NAME,
dwh_22.d_membername.membername,
dwh_22.d_memberdisplayname.memberdisplayname,
dwh_22.c_miembros.membershipavailability,
dwh_22.d_membership_beanrolename.membership_beanrolename,
( ( 8 * dwh_22.c_miembros.membershipavailability ) / 100 ),
planificacion.FechaInicioPlan,
planificacion.FechaTerminoPlan,
planificacion.HorasPlanificadosPorDia,
jira.planificacion_original,
planificacion.codigo,
planificacion.Nombre,
planificacion.UsuarioAsignado,
planificacion.UsuarioAsignadoSistema,
planificacion.EstadoIssue,
planificacion.PlanificacionCreadaPor_USER,
planificacion.PlanificacionCreadaPor_KEY,
jira.incidente_padre,
jira.descripcion_origen,
jira.estado_incidente_padre,
jira.usuario_jefe_de_fabrica,
jira.usuario_lider_de_fabrica,
jira.usuario_lider_proyecto,
jira.incidente_hijo,
jira.descripcion_destino,
jira.comite_estimado,
jira.funnel 
) AS plantotal ON horastotalesequipos.diaPlanificado = plantotal.diaPlanificado 
AND horastotalesequipos.dw = plantotal.dw 
AND horastotalesequipos.NAME = plantotal.Departamento 
WHERE
horastotalesequipos.dw NOT IN ( 6, 0 )
) AS plantotalhoras