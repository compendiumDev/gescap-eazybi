/*
 Navicat Premium Data Transfer

 Source Server         : BCH Atlassian Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 90325
 Source Host           : 152.139.147.122:5432
 Source Catalog        : eazybi_qa
 Source Schema         : dwh_24

 Target Server Type    : PostgreSQL
 Target Server Version : 90325
 File Encoding         : 65001

 Date: 23/07/2020 11:52:29
*/


-- ----------------------------
-- Table structure for d_feriados
-- ----------------------------
DROP TABLE IF EXISTS "dwh_24"."d_feriados";
CREATE TABLE "dwh_24"."d_feriados" (
  "id" int4 NOT NULL,
  "feriado" timestamp(6) NOT NULL
)
;

-- ----------------------------
-- Records of d_feriados
-- ----------------------------
INSERT INTO "dwh_24"."d_feriados" VALUES (1, '2020-01-01 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (2, '2020-04-10 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (3, '2020-05-01 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (4, '2020-05-21 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (5, '2020-06-29 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (6, '2020-07-16 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (7, '2020-08-15 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (8, '2020-09-18 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (9, '2020-09-19 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (10, '2020-10-12 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (11, '2020-12-08 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (12, '2020-12-25 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (13, '2021-01-01 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (14, '2021-04-02 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (15, '2021-05-21 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (16, '2021-06-28 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (17, '2021-07-16 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (18, '2021-10-11 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (19, '2021-11-01 00:00:00');
INSERT INTO "dwh_24"."d_feriados" VALUES (20, '2021-12-08 00:00:00');

-- ----------------------------
-- Primary Key structure for table d_feriados
-- ----------------------------
ALTER TABLE "dwh_24"."d_feriados" ADD CONSTRAINT "d_feriados_pkey" PRIMARY KEY ("id");
