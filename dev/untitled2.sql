(
		SELECT 	tbl_factor.summary,
				tbl_factor."lead",
				tbl_factor.diaCalendario,
				COUNT(tbl_factor.diaCalendario) 								cuentaDias,
				SUM(tbl_factor.cantPersonas) 									cuentaPersonas,
				SUM(tbl_factor.cantPersonas) / COUNT(tbl_factor.diaCalendario) 	RecursosMesFabrica 
		FROM
			(
			SELECT 	generate_series ( MIN ( '2020-01-01' :: TIMESTAMP ), MAX ( '2025-12-31' :: TIMESTAMP ), INTERVAL '24 hour' ) AS diaCalendario,
					EXTRACT ( DOW FROM generate_series ( MIN ( '2020-01-01' :: TIMESTAMP ), MAX ( '2025-12-31' :: TIMESTAMP ), INTERVAL '24 hour' ) ) AS dwDiaCalendario,
					EQUIPOS.summary,
					EQUIPOS."lead",
					COUNT ( equipos.membername ) cantPersonas 
			FROM
				(
				SELECT
					dwh_22.c_equipos."id", dwh_22.c_equipos.name_id, dwh_22.c_equipos.summary_id, dwh_22.c_equipos.lead_id, dwh_22.d_name."name",
					dwh_22.d_summary.summary, dwh_22.d_lead."lead", dwh_22.d_membername.membername, dwh_22.c_miembros.membershipteam,
					dwh_22.d_membershiprolename.membershiprolename 
				FROM
					dwh_22.c_equipos
					INNER JOIN dwh_22.d_name ON dwh_22.c_equipos.name_id = dwh_22.d_name."id"
					INNER JOIN dwh_22.d_summary ON dwh_22.c_equipos.summary_id = dwh_22.d_summary."id"
					INNER JOIN dwh_22.d_lead ON dwh_22.c_equipos.lead_id = dwh_22.d_lead."id"
					INNER JOIN dwh_22.c_miembros ON dwh_22.c_equipos."id" = dwh_22.c_miembros.membershipteam
					INNER JOIN dwh_22.d_membername ON dwh_22.c_miembros.membername_id = dwh_22.d_membername."id"
					INNER JOIN dwh_22.d_membershiprolename ON dwh_22.c_miembros.membershiprolename_id = dwh_22.d_membershiprolename."id" 
				WHERE
					dwh_22.d_membershiprolename.membershiprolename IN ( 'Analista de Sistemas', 'Jefe de Proyectos' ) 
				) EQUIPOS 
			GROUP BY EQUIPOS.summary, EQUIPOS."lead" 
			) 
		tbl_factor 
		WHERE
			tbl_factor.dwDiaCalendario NOT IN ( 6, 0 ) AND tbl_factor.diaCalendario NOT IN ( SELECT feriado FROM dwh_24.d_feriados ) 
		GROUP BY
			tbl_factor.summary, tbl_factor."lead", tbl_factor.diaCalendario 
		ORDER BY
			3,4 
		) tbl_time