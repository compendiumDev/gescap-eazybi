SELECT
MAINCONTROL.incidente_padre, 			MAINCONTROL.membername, 			MAINCONTROL.descripcion_origen,		 	MAINCONTROL.estado_incidente_padre	, 
MAINCONTROL.usuario_lider_de_fabrica, 	MAINCONTROL.summary	, 				MAINCONTROL.usuario_jefe_de_fabrica, 	MAINCONTROL."lead", 
MAINCONTROL.usuario_lider_proyecto, 	MAINCONTROL.incidente_hijo, 		MAINCONTROL.codigo, 					MAINCONTROL.descripcion_destino, 
MAINCONTROL.comite_estimado, 			MAINCONTROL.funnel, 				MAINCONTROL.status_avance_color, 		MAINCONTROL.membershiprolename, 
MAINCONTROL."LIDER O APOYO", 			MAINCONTROL.name, 					MAINCONTROL.usuarioasignado, 			MAINCONTROL.usuarioasignadosistema, 
MAINCONTROL.estadoissue, 				MAINCONTROL.diaPlanificado, 		MAINCONTROL.fechainicioplan, 			MAINCONTROL.fechaterminoplan, 
MAINCONTROL.horasplanificadospordia, 	MAINCONTROL.planificacion_original, MAINCONTROL.diaCalendario, 				MAINCONTROL.RecursosMesFabrica,
MAINCONTROL.cuentaDias, 				MAINCONTROL.cuentaPersonas
FROM
	(
	SELECT
		tbl_time.diaCalendario,
		tbl_time.RecursosMesFabrica,
		tbl_time.cuentaDias,
		tbl_time.cuentaPersonas,
	CASE WHEN principal.incidente_padre 			IS NULL THEN '-' 				ELSE principal.incidente_padre END 				AS incidente_padre,
	CASE WHEN principal.membername 					IS NULL THEN '-' 				ELSE principal.membername END 					AS membername,
	CASE WHEN principal.descripcion_origen 			IS NULL THEN '-' 				ELSE principal.descripcion_origen END 			AS descripcion_origen,
	CASE WHEN principal.estado_incidente_padre 		IS NULL THEN '-' 				ELSE principal.estado_incidente_padre END 		AS estado_incidente_padre,
	CASE WHEN principal.usuario_lider_de_fabrica 	IS NULL THEN '-' 				ELSE principal.usuario_lider_de_fabrica  END 	AS usuario_lider_de_fabrica,
	CASE WHEN principal.summary 					IS NULL THEN tbl_time.summary 	ELSE principal.summary END 						AS summary,
	CASE WHEN principal.usuario_jefe_de_fabrica 	IS NULL THEN '-' 				ELSE principal.usuario_jefe_de_fabrica END 		AS usuario_jefe_de_fabrica,
	CASE WHEN principal."lead" 						IS NULL THEN tbl_time."lead" 	ELSE principal."lead" END 						AS "lead",
	CASE WHEN principal.usuario_lider_proyecto 		IS NULL THEN '-' 				ELSE principal.usuario_lider_proyecto END 		AS usuario_lider_proyecto,
	CASE WHEN principal.incidente_hijo 				IS NULL THEN '-' 				ELSE principal.incidente_hijo END 				AS incidente_hijo,
	CASE WHEN principal.codigo 						IS NULL THEN '-' 				ELSE principal.codigo END 						AS codigo,
	CASE WHEN principal.descripcion_destino 		IS NULL THEN '-' 				ELSE principal.descripcion_destino END 			AS descripcion_destino,
	CASE WHEN principal.comite_estimado 			IS NULL THEN '-' 				ELSE principal.comite_estimado END 				AS comite_estimado,
	CASE WHEN principal.funnel 						IS NULL THEN '-' 				ELSE principal.funnel END 						AS funnel,
	CASE WHEN principal.status_avance_color 		IS NULL THEN '-' 				ELSE principal.status_avance_color END 			AS status_avance_color,
	CASE WHEN principal.membershiprolename 			IS NULL THEN '-' 				ELSE principal.membershiprolename END 			AS membershiprolename,
	CASE WHEN principal."LIDER O APOYO" 			IS NULL THEN '-' 				ELSE principal."LIDER O APOYO" END 				AS "LIDER O APOYO",
	CASE WHEN principal.NAME 						IS NULL THEN '-' 				ELSE principal.NAME END 						AS NAME,
	CASE WHEN principal.usuarioasignado 			IS NULL THEN '-' 				ELSE principal.usuarioasignado END 				AS usuarioasignado,
	CASE WHEN principal.usuarioasignadosistema 		IS NULL THEN '-' 				ELSE principal.usuarioasignadosistema END 		AS usuarioasignadosistema,
	CASE WHEN principal.estadoissue 				IS NULL THEN '-' 				ELSE principal.estadoissue END 					AS estadoissue,
	CASE WHEN principal.diaPlanificado 				IS NULL THEN '1900-01-01' 		ELSE principal.diaPlanificado END 				AS diaPlanificado,
	CASE WHEN principal.fechainicioplan 			IS NULL THEN '1900-01-01' 		ELSE principal.fechainicioplan END 				AS fechainicioplan,
	CASE WHEN principal.fechaterminoplan 			IS NULL THEN '1900-01-01' 		ELSE principal.fechaterminoplan END 			AS fechaterminoplan,
	CASE WHEN principal.horasplanificadospordia 	IS NULL THEN 0 					ELSE principal.horasplanificadospordia END 		AS horasplanificadospordia,
	CASE WHEN principal.planificacion_original 		IS NULL THEN '0' 				ELSE principal.planificacion_original END 		AS planificacion_original 
	FROM(

		SELECT
			dwh_22.d_incidente_padre.incidente_padre
		,	dwh_22.d_descripcion_origen.descripcion_origen
		, 	dwh_22.d_estado_incidente_padre.estado_incidente_padre	
		, 	dwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica
		,	planificacion.summary	
		, 	dwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica
		,	planificacion.lead
		, 	dwh_22.d_usuario_lider_proyecto.usuario_lider_proyecto
		, 	dwh_22.d_incidente_hijo.incidente_hijo
		,	planificacion.parent_key codigo
		, 	planificacion.issue_key subtarea
		, 	dwh_22.d_descripcion_destino.descripcion_destino
		, 	dwh_22.d_comite_estimado.comite_estimado
		, 	dwh_22.d_funnel.funnel
		, 	dwh_22.d_status_avance_color.status_avance_color
		,	planificacion.membername
		,	planificacion.membershiprolename
		,	planificacion.name
		,	planificacion.username usuarioasignado
		,	planificacion.staff usuarioasignadosistema
		,	planificacion.diaPlanificado
		,	planificacion.hours horasplanificadospordia

		FROM
			dwh_22.c_data_jira
			LEFT JOIN dwh_22.d_descripcion_origen 				ON dwh_22.c_data_jira.descripcion_origen_id 			= dwh_22.d_descripcion_origen."id"
			LEFT JOIN dwh_22.d_incidente_hijo 					ON dwh_22.c_data_jira.incidente_hijo_id 				= dwh_22.d_incidente_hijo."id"
			LEFT JOIN dwh_22.d_descripcion_destino 				ON dwh_22.c_data_jira.descripcion_destino_id 			= dwh_22.d_descripcion_destino."id"
			LEFT JOIN dwh_22.d_incidente_padre 					ON dwh_22.c_data_jira.incidente_padre_id 				= dwh_22.d_incidente_padre."id"
			LEFT JOIN dwh_22.d_estado_incidente_padre 			ON dwh_22.c_data_jira.estado_incidente_padre_id 		= dwh_22.d_estado_incidente_padre."id"
			LEFT JOIN dwh_22.d_estado_incidente_hijo 			ON dwh_22.c_data_jira.estado_incidente_hijo_id 			= dwh_22.d_estado_incidente_hijo."id"
			LEFT JOIN dwh_22.d_usuario_jefe_de_fabrica 			ON dwh_22.c_data_jira.usuario_jefe_de_fabrica_id 		= dwh_22.d_usuario_jefe_de_fabrica."id"
			LEFT JOIN dwh_22.d_usuario_lider_de_fabrica 		ON dwh_22.c_data_jira.usuario_lider_de_fabrica_id 		= dwh_22.d_usuario_lider_de_fabrica."id"
			LEFT JOIN dwh_22.d_usuario_lider_proyecto 			ON dwh_22.c_data_jira.usuario_lider_proyecto_id 		= dwh_22.d_usuario_lider_proyecto."id"
			LEFT JOIN dwh_22.d_funnel 							ON dwh_22.c_data_jira.funnel_id 						= dwh_22.d_funnel."id"
			LEFT JOIN dwh_22.d_comite_estimado 					ON dwh_22.c_data_jira.comite_estimado_id 				= dwh_22.d_comite_estimado."id"
			LEFT JOIN dwh_22.d_status_avance_color 				ON dwh_22.c_data_jira.status_avance_color_id 			= dwh_22.d_status_avance_color."id"
			LEFT JOIN 
		(
			SELECT
				dwh_22.d_membername.membername
			,	dwh_22.d_membershiprolename.membershiprolename
			,	dwh_22.d_name."name"
			,	dwh_22.d_summary.summary
			,	dwh_22.d_lead."lead"
			,	logeohoras.time_value diaPlanificado
			,	logeohoras.parent_key
			,	logeohoras.issue_key
			,	logeohoras.username
			,	logeohoras.staff
			,	logeohoras.hours hours
	
			FROM
				dwh_22.c_miembros
				INNER JOIN dwh_22.d_membername 						ON dwh_22.c_miembros.membername_id 					= dwh_22.d_membername."id"
				INNER JOIN dwh_22.d_membershiprolename 		ON dwh_22.c_miembros.membershiprolename_id 	= dwh_22.d_membershiprolename."id"
				INNER JOIN dwh_22.c_equipos 							ON dwh_22.c_miembros.membershipteam 				= dwh_22.c_equipos."id"
				INNER JOIN dwh_22.d_name 									ON dwh_22.c_equipos.name_id 								= dwh_22.d_name."id"
				INNER JOIN dwh_22.d_summary 							ON dwh_22.c_equipos.summary_id 							= dwh_22.d_summary."id"
				INNER JOIN dwh_22.d_lead 									ON dwh_22.c_equipos.lead_id 								= dwh_22.d_lead."id" 
				LEFT JOIN 
			 	(
			 			SELECT
			 					dwh_22.d_parent_key.parent_key,
			 					dwh_22.d_issue_key.issue_key,
			 					dwh_22.d_time.time_value,
			 					dwh_22.d_username.username,
			 					dwh_22.d_staff.staff,
			 					sum(dwh_22.c_horasregistradas.hours) hours
			 			FROM
			 				dwh_22.c_horasregistradas
			 				INNER JOIN dwh_22.d_staff 			ON dwh_22.c_horasregistradas.staff_id 			= dwh_22.d_staff."id"
			 				INNER JOIN dwh_22.d_username 		ON dwh_22.c_horasregistradas.username_id 		= dwh_22.d_username."id"
			 				INNER JOIN dwh_22.d_time 				ON dwh_22.c_horasregistradas.time_id 				= dwh_22.d_time."id"
			 				INNER JOIN dwh_22.d_issue_key 	ON dwh_22.c_horasregistradas.issue_key_id 	= dwh_22.d_issue_key."id"
			 				INNER JOIN dwh_22.d_parent_key 	ON dwh_22.c_horasregistradas.parent_key_id 	= dwh_22.d_parent_key."id" 
			 			WHERE
			 				dwh_22.d_issue_key.issue_key LIKE'PMO-%' 
							group by 
							
								dwh_22.d_parent_key.parent_key,
			 					dwh_22.d_issue_key.issue_key,
			 					dwh_22.d_time.time_value,
			 					dwh_22.d_username.username,
			 					dwh_22.d_staff.staff
			 	) 
			 	logeohoras
			 				on 	dwh_22.d_membername.membername 	= logeohoras.username
			group by 
					dwh_22.d_membername.membername
				,	dwh_22.d_membershiprolename.membershiprolename
				,	dwh_22.d_name."name"
				,	dwh_22.d_summary.summary
				,	dwh_22.d_lead."lead"
				,	logeohoras.time_value
				,	logeohoras.parent_key
				,	logeohoras.issue_key
				,	logeohoras.username
				,	logeohoras.staff
				,	logeohoras.time_value
				,	logeohoras.hours
			
			) as 
			planificacion 		on 	 planificacion.parent_key 	= dwh_22.d_incidente_hijo.incidente_hijo
								and	 planificacion.summary 		= dwh_22.d_usuario_lider_de_fabrica.usuario_lider_de_fabrica
								and  planificacion.lead 		= dwh_22.d_usuario_jefe_de_fabrica.usuario_jefe_de_fabrica
		

	WHERE
	tbl_time.diaCalendario NOT IN ( SELECT feriado FROM dwh_24.d_feriados ) 
	) maincontrol
